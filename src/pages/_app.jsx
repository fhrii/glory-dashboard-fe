import { Providers } from '@/providers';

export default function App({ Component, pageProps }) {
  const getLayout = Component.getLayout || ((page) => page);

  return <Providers>{getLayout(<Component {...pageProps} />)}</Providers>;
}
