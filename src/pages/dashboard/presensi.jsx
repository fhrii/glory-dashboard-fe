import { Grid, GridItem } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import { useCallback, useEffect } from 'react';

import { DashboardLayout } from '@/components/Layout';
import {
  EmployeePresenceLineChart,
  EmployeePresenceList,
  EmployeePresencePieChart,
} from '@/features/employee';
import { useBoundStore } from '@/store';

export default function AbsensiKaryawan() {
  const [setDashboardMenuName, setDashboardBreadCrumbs] = useBoundStore(
    useCallback(
      (state) => [state.setDashboardMenuName, state.setDashboardBreadCrumbs],
      []
    )
  );

  useEffect(() => {
    setDashboardMenuName('Presensi Karyawan');
    setDashboardBreadCrumbs([
      { name: 'Presensi Karyawan', href: '/dashboard/presensi' },
    ]);
  }, []);

  return (
    <Grid templateColumns="repeat(6, 1fr)" gap="6">
      <NextSeo title="Presensi Karyawan | Dashboard" />
      <GridItem colSpan={4}>
        <EmployeePresenceLineChart />
      </GridItem>
      <GridItem colSpan={2}>
        <EmployeePresencePieChart />
      </GridItem>
      <GridItem colSpan={6}>
        <EmployeePresenceList />
      </GridItem>
    </Grid>
  );
}

AbsensiKaryawan.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;
