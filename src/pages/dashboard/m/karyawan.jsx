import { NextSeo } from 'next-seo';
import { useCallback, useEffect } from 'react';

import { DashboardLayout } from '@/components/Layout';
import { EmployeeList } from '@/features/employee';
import { useBoundStore } from '@/store';

export default function MasterKaryawan() {
  const [setDashboardMenuName, setDashboardBreadCrumbs] = useBoundStore(
    useCallback(
      (state) => [state.setDashboardMenuName, state.setDashboardBreadCrumbs],
      []
    )
  );

  useEffect(() => {
    setDashboardMenuName('Master Karyawan');
    setDashboardBreadCrumbs([
      { name: 'Master', href: '/dashboard/m/karyawan' },
      { name: 'Karyawan', href: '/dashboard/m/karyawan' },
    ]);
  }, []);

  return (
    <>
      <NextSeo title="Master Karyawan | Dashboard" />
      <EmployeeList />
    </>
  );
}

MasterKaryawan.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;
