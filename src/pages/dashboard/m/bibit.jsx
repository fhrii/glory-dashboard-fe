import { NextSeo } from 'next-seo';
import { useCallback, useEffect } from 'react';

import { DashboardLayout } from '@/components/Layout';
import { SeedList } from '@/features/seed';
import { useBoundStore } from '@/store';

export default function MasterBibit() {
  const [setDashboardMenuName, setDashboardBreadCrumbs] = useBoundStore(
    useCallback(
      (state) => [state.setDashboardMenuName, state.setDashboardBreadCrumbs],
      []
    )
  );

  useEffect(() => {
    setDashboardMenuName('Master Bibit');
    setDashboardBreadCrumbs([
      { name: 'Master', href: '/dashboard/m/bibit' },
      { name: 'Bibit', href: '/dashboard/m/bibit' },
    ]);
  }, []);

  return (
    <>
      <NextSeo title="Master Bibit | Dashboard" />
      <SeedList />
    </>
  );
}

MasterBibit.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;
