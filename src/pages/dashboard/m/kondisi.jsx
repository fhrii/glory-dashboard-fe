import { NextSeo } from 'next-seo';
import { useCallback, useEffect } from 'react';

import { DashboardLayout } from '@/components/Layout';
import { ConditionList } from '@/features/condition';
import { useBoundStore } from '@/store';

export default function MasterCondition() {
  const [setDashboardMenuName, setDashboardBreadCrumbs] = useBoundStore(
    useCallback(
      (state) => [state.setDashboardMenuName, state.setDashboardBreadCrumbs],
      []
    )
  );

  useEffect(() => {
    setDashboardMenuName('Master Kondisi');
    setDashboardBreadCrumbs([
      { name: 'Master', href: '/dashboard/m/kondisi' },
      { name: 'Kondisi', href: '/dashboard/m/kondisi' },
    ]);
  }, []);

  return (
    <>
      <NextSeo title="Master Kondisi | Dashboard" />
      <ConditionList />
    </>
  );
}

MasterCondition.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;
