import { NextSeo } from 'next-seo';
import { useCallback, useEffect } from 'react';

import { DashboardLayout } from '@/components/Layout';
import { BlockList } from '@/features/block';
import { useBoundStore } from '@/store';

export default function MasterBlok() {
  const [setDashboardMenuName, setDashboardBreadCrumbs] = useBoundStore(
    useCallback(
      (state) => [state.setDashboardMenuName, state.setDashboardBreadCrumbs],
      []
    )
  );

  useEffect(() => {
    setDashboardMenuName('Master Blok');
    setDashboardBreadCrumbs([
      { name: 'Master', href: '/dashboard/m/blok' },
      { name: 'Blok', href: '/dashboard/m/blok' },
    ]);
  }, []);

  return (
    <>
      <NextSeo title="Master Blok | Dashboard" />
      <BlockList />
    </>
  );
}

MasterBlok.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;
