import { Grid, GridItem } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import { useCallback, useEffect } from 'react';

import { DashboardLayout } from '@/components/Layout';
import { EmployeePresencePieChart } from '@/features/employee';
import { HarvestPieChart } from '@/features/harvest';
import { PlantPieChart } from '@/features/plant';
import { useBoundStore } from '@/store';

export default function Overview() {
  const [setDashboardMenuName, setDashboardBreadCrumbs] = useBoundStore(
    useCallback(
      (state) => [state.setDashboardMenuName, state.setDashboardBreadCrumbs],
      []
    )
  );

  useEffect(() => {
    setDashboardMenuName('Overview');
    setDashboardBreadCrumbs([{ name: 'Overview', href: '/dashboard' }]);
  }, []);

  return (
    <Grid templateColumns="repeat(6, 1fr)" gap="6">
      <NextSeo title="Overview | Dashboard" />
      <GridItem colSpan={2}>
        <EmployeePresencePieChart label="Presensi Hari Ini" />
      </GridItem>
      <GridItem colSpan={2}>
        <PlantPieChart label="Tanam Hari Ini" />
      </GridItem>
      <GridItem colSpan={2}>
        <HarvestPieChart label="Panen Hari Ini" />
      </GridItem>
    </Grid>
  );
}

Overview.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;
