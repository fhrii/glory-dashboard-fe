import { Grid, GridItem } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import { useCallback, useEffect } from 'react';

import { DashboardLayout } from '@/components/Layout';
import { PlantLineChart, PlantList, PlantPieChart } from '@/features/plant';
import { useBoundStore } from '@/store';

export default function SetorTanam() {
  const [setDashboardMenuName, setDashboardBreadCrumbs] = useBoundStore(
    useCallback(
      (state) => [state.setDashboardMenuName, state.setDashboardBreadCrumbs],
      []
    )
  );

  useEffect(() => {
    setDashboardMenuName('Setor Tanam');
    setDashboardBreadCrumbs([
      { name: 'Setor', href: '/dashboard/s/tanam' },
      { name: 'Tanam', href: '/dashboard/s/tanam' },
    ]);
  }, []);

  return (
    <Grid templateColumns="repeat(6, 1fr)" gap="6">
      <NextSeo title="Setor Tanam | Dashboard" />
      <GridItem colSpan={4}>
        <PlantLineChart />
      </GridItem>
      <GridItem colSpan={2}>
        <PlantPieChart />
      </GridItem>
      <GridItem colSpan={6}>
        <PlantList />
      </GridItem>
    </Grid>
  );
}

SetorTanam.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;
