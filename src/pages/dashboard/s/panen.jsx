import { Grid, GridItem } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';
import { useCallback, useEffect } from 'react';

import { DashboardLayout } from '@/components/Layout';
import {
  HarvestLineChart,
  HarvestList,
  HarvestPieChart,
} from '@/features/harvest';
import { useBoundStore } from '@/store';

export default function SetorPanen() {
  const [setDashboardMenuName, setDashboardBreadCrumbs] = useBoundStore(
    useCallback(
      (state) => [state.setDashboardMenuName, state.setDashboardBreadCrumbs],
      []
    )
  );

  useEffect(() => {
    setDashboardMenuName('Setor Panen');
    setDashboardBreadCrumbs([
      { name: 'Setor', href: '/dashboard/s/panen' },
      { name: 'Panen', href: '/dashboard/s/panen' },
    ]);
  }, []);

  return (
    <Grid templateColumns="repeat(6, 1fr)" gap="6">
      <NextSeo title="Setor Panen | Dashboard" />
      <GridItem colSpan={4}>
        <HarvestLineChart />
      </GridItem>
      <GridItem colSpan={2}>
        <HarvestPieChart />
      </GridItem>
      <GridItem colSpan={6}>
        <HarvestList />
      </GridItem>
    </Grid>
  );
}

SetorPanen.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;
