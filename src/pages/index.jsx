import { Box, Flex } from '@chakra-ui/react';
import { NextSeo } from 'next-seo';

import { AuthLayout } from '@/components/Layout';
import { Login } from '@/features/auth';

export default function Home() {
  return (
    <Flex
      padding="4"
      minHeight="100vh"
      justifyContent="center"
      alignItems="center"
    >
      <NextSeo title="Dashboard" />
      <Box
        bgColor="white"
        border="1px"
        borderColor="gray.200"
        padding="4"
        rounded="lg"
        width="full"
        maxWidth="lg"
      >
        <Login />
      </Box>
    </Flex>
  );
}

Home.getLayout = (page) => <AuthLayout>{page}</AuthLayout>;
