import { QueryClientProvider } from 'react-query';

import { queryClient } from '@/libs/react-query';
import { ToastContainer } from '@/utils/toast';

import { ChakraProvider } from './ChakraProvider';

export function Providers({ children }) {
  return (
    <ChakraProvider>
      <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
      <ToastContainer />
    </ChakraProvider>
  );
}
