import { ChakraProvider as CP } from '@chakra-ui/react';

export function ChakraProvider({ children }) {
  return <CP>{children}</CP>;
}
