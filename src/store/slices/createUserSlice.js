export const createUserSlice = (set) => ({
  accessToken: null,
  isAuthenticated: false,
  addAccessToken: (accessToken) =>
    set(() => ({ accessToken, isAuthenticated: true })),
  removeAccessToken: () =>
    set(() => ({ accessToken: null, isAuthenticated: false })),
});
