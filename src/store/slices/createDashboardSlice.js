export const createDashboardSlice = (set) => ({
  dashboardMenuName: 'Overview',
  dashboardBreadcrumbs: [
    {
      name: 'Dashboard',
      href: '/dashboard',
    },
    {
      name: 'Overview',
      href: '/dashboard',
    },
  ],
  isDrawerMenuShouldActive: true,
  activateDrawerMenu: () => set(() => ({ isDrawerMenuShouldActive: true })),
  deactivateDrawerMenu: () => set(() => ({ isDrawerMenuShouldActive: false })),
  setDashboardMenuName: (dashboardMenuName) =>
    set(() => ({ dashboardMenuName })),
  setDashboardBreadCrumbs: (breadcrumbs) =>
    set((state) => ({
      dashboardBreadcrumbs: [state.dashboardBreadcrumbs[0], ...breadcrumbs],
    })),
});
