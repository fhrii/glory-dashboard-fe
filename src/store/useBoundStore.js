import { create } from 'zustand';
import { createJSONStorage, persist } from 'zustand/middleware';

import { createDashboardSlice, createUserSlice } from './slices';

export const useBoundStore = create(
  persist(
    (...zProps) => ({
      ...createUserSlice(...zProps),
      ...createDashboardSlice(...zProps),
    }),
    {
      name: 'app',
      storage: createJSONStorage(() => localStorage),
      partialize: (state) => ({
        accessToken: state.accessToken,
        isAuthenticated: state.isAuthenticated,
      }),
    }
  )
);
