import { Box, Icon, useStyleConfig } from '@chakra-ui/react';
import { AiOutlineArrowLeft, AiOutlineArrowRight } from 'react-icons/ai';
import ReactPaginate from 'react-paginate';

export function Pagination({ onPageChange, totalPages }) {
  const buttonStyles = useStyleConfig('Button', {
    variant: 'outline',
  });

  return (
    <Box
      sx={{
        '.paginate-container': {
          listStyle: 'none',
          gap: '2',
          display: 'flex',
        },
        '.paginate-button > a': {
          ...buttonStyles,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        },
        '.paginate-button.selected > a': {
          color: 'white',
          bgColor: 'gray.600',
        },
      }}
    >
      <ReactPaginate
        breakLabel="..."
        nextLabel={<Icon as={AiOutlineArrowRight} />}
        previousLabel={<Icon as={AiOutlineArrowLeft} />}
        containerClassName="paginate-container"
        previousClassName="paginate-button"
        nextClassName="paginate-button"
        pageClassName="paginate-button"
        onPageChange={onPageChange}
        pageRangeDisplayed={3}
        marginPagesDisplayed={1}
        pageCount={totalPages || 0}
        renderOnZeroPageCount={null}
      />
    </Box>
  );
}
