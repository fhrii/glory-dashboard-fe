import { Box } from '@chakra-ui/react';
import { zodResolver } from '@hookform/resolvers/zod';
import { useForm as useReactHookForm } from 'react-hook-form';

export function Form({ children, id, withForm, onSubmit }) {
  const methods = withForm;

  return (
    <Box as="form" onSubmit={methods.handleSubmit(onSubmit)} id={id} noValidate>
      {children(methods)}
    </Box>
  );
}

export function useForm(schema, props) {
  return useReactHookForm({
    ...props,
    resolver: schema ? zodResolver(schema) : undefined,
  });
}
