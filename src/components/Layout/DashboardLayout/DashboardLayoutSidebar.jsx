import { Divider, HStack, Icon, Text, VStack } from '@chakra-ui/react';
import { useCallback } from 'react';
import { AiOutlineDashboard } from 'react-icons/ai';

import { useBoundStore } from '@/store';

import { DashboardLayoutSideMenu } from './DashboardLayoutSideMenu';

export function DashboardLayoutSidebar() {
  const isDrawerMenuShouldActive = useBoundStore(
    useCallback((state) => state.isDrawerMenuShouldActive, [])
  );

  if (isDrawerMenuShouldActive) return null;

  return (
    <VStack
      as="aside"
      paddingX="8"
      width="xs"
      alignItems="stretch"
      bg="white"
      borderRight="1px"
      borderRightColor="gray.200"
    >
      <HStack paddingY="8" justify="center">
        <Icon as={AiOutlineDashboard} fontSize="2xl" />
        <Text as="h1" fontWeight="medium" fontSize="lg">
          PT Glory Argo Indonesia
        </Text>
      </HStack>
      <Divider bgColor="gray.600" />
      <DashboardLayoutSideMenu />
    </VStack>
  );
}
