import {
  Box,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Drawer,
  DrawerBody,
  DrawerContent,
  DrawerOverlay,
  Heading,
  HStack,
  Icon,
  IconButton,
  Spacer,
  useBreakpointValue,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { useCallback, useEffect, useRef } from 'react';
import { AiOutlineMenu } from 'react-icons/ai';

import { useBoundStore } from '@/store';

import { DashboardLayoutSideMenu } from './DashboardLayoutSideMenu';

export function DashboardLayoutNavbar() {
  const { pathname } = useRouter();
  const [dashboardMenuName, dashboardBreadcrumbs] = useBoundStore(
    useCallback(
      (state) => [state.dashboardMenuName, state.dashboardBreadcrumbs],
      []
    )
  );
  const btnMenuRef = useRef();
  const isDrawerMenuActive = useBreakpointValue(
    { base: true, md: false },
    { fallback: 'md' }
  );
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [activateDrawerMenu, deactivateDrawerMenu] = useBoundStore(
    useCallback(
      (state) => [state.activateDrawerMenu, state.deactivateDrawerMenu],
      []
    )
  );

  useEffect(() => {
    if (isDrawerMenuActive) activateDrawerMenu();
    else deactivateDrawerMenu();
  }, [isDrawerMenuActive]);

  useEffect(() => {
    if (isOpen) onClose();
  }, [pathname]);

  return (
    <Box zIndex="1">
      <HStack
        p="4"
        bgColor="white"
        border="1px"
        borderColor="gray.200"
        rounded="lg"
      >
        <VStack alignItems="stretch">
          <Breadcrumb separator="/" fontSize="sm" color="gray.500">
            {dashboardBreadcrumbs.map((breadcrumb, index) => (
              <BreadcrumbItem
                key={index}
                color={
                  dashboardBreadcrumbs.length - 1 === index ? 'initial' : null
                }
                isCurrentPage={dashboardBreadcrumbs.length - 1 === index}
              >
                <BreadcrumbLink href={breadcrumb.href}>
                  {breadcrumb.name}
                </BreadcrumbLink>
              </BreadcrumbItem>
            ))}
          </Breadcrumb>
          <Heading as="h2" size="sm">
            {dashboardMenuName}
          </Heading>
        </VStack>
        <Spacer />
        {isDrawerMenuActive && (
          <>
            <IconButton
              ref={btnMenuRef}
              icon={<Icon as={AiOutlineMenu} />}
              variant="ghost"
              onClick={onOpen}
              aria-label="Menu"
            />
            <Drawer
              isOpen={isOpen}
              placement="left"
              onClose={onClose}
              finalFocusRef={btnMenuRef}
            >
              <DrawerOverlay />
              <DrawerContent>
                <DrawerBody>
                  <DashboardLayoutSideMenu />
                </DrawerBody>
              </DrawerContent>
            </Drawer>
          </>
        )}
      </HStack>
    </Box>
  );
}
