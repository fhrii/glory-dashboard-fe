import {
  Button,
  Collapse,
  Icon,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
} from 'react';
import {
  AiOutlineBarChart,
  AiOutlineLogout,
  AiOutlineNodeIndex,
  AiOutlinePlusCircle,
  AiOutlineUser,
} from 'react-icons/ai';

import { useBoundStore } from '@/store';

export function DashboardLayoutSideMenu() {
  const removeAccessToken = useBoundStore(
    useCallback((state) => state.removeAccessToken, [])
  );

  return (
    <VStack alignItems="stretch">
      <MenuButtonLink href="/dashboard" icon={AiOutlineBarChart}>
        Overview
      </MenuButtonLink>
      <MenuCollapse activeQuery="/dashboard/m/">
        <MenuCollapseButton icon={AiOutlineNodeIndex}>
          Master
        </MenuCollapseButton>
        <MenuCollapseContent>
          <MenuCollapseSubMenu href="/dashboard/m/karyawan">
            Karyawan
          </MenuCollapseSubMenu>
          <MenuCollapseSubMenu href="/dashboard/m/bibit">
            Bibit
          </MenuCollapseSubMenu>
          <MenuCollapseSubMenu href="/dashboard/m/kondisi">
            Kondisi
          </MenuCollapseSubMenu>
          <MenuCollapseSubMenu href="/dashboard/m/blok">
            Blok
          </MenuCollapseSubMenu>
        </MenuCollapseContent>
      </MenuCollapse>
      <MenuButtonLink href="/dashboard/presensi" icon={AiOutlineUser}>
        Presensi Karyawan
      </MenuButtonLink>
      <MenuCollapse activeQuery="/dashboard/s/">
        <MenuCollapseButton icon={AiOutlinePlusCircle}>
          Setor
        </MenuCollapseButton>
        <MenuCollapseContent>
          <MenuCollapseSubMenu href="/dashboard/s/tanam">
            Tanam
          </MenuCollapseSubMenu>
          <MenuCollapseSubMenu href="/dashboard/s/panen">
            Panen
          </MenuCollapseSubMenu>
        </MenuCollapseContent>
      </MenuCollapse>
      <MenuButtonNotLink
        icon={AiOutlineLogout}
        isLink="false"
        onClick={removeAccessToken}
      >
        Keluar
      </MenuButtonNotLink>
    </VStack>
  );
}

function MenuButtonLink({ children, href, icon }) {
  const { pathname } = useRouter();
  const isActive = useMemo(() => pathname === href, [pathname]);

  return (
    <Link href={href} passHref legacyBehavior>
      <Button
        as="a"
        leftIcon={icon ? <Icon as={icon} /> : null}
        variant="ghost"
        justifyContent="start"
        bgColor={isActive ? 'gray.600' : null}
        color={isActive ? 'white' : null}
        _hover={{ bgColor: isActive ? 'gray.700' : 'inherit' }}
      >
        {children}
      </Button>
    </Link>
  );
}

function MenuButtonNotLink({ children, icon, isActive, onClick }) {
  return (
    <Button
      leftIcon={icon ? <Icon as={icon} /> : null}
      variant="ghost"
      justifyContent="start"
      cursor="pointer"
      bgColor={isActive ? 'gray.600' : null}
      color={isActive ? 'white' : null}
      onClick={onClick}
      _hover={{ bgColor: isActive ? 'gray.700' : 'inherit' }}
    >
      {children}
    </Button>
  );
}

const MenuCollapseContext = createContext({});

const useMenuCollapse = () => useContext(MenuCollapseContext);

function MenuCollapse({ children, activeQuery }) {
  const { pathname } = useRouter();
  const isActive = useMemo(() => pathname.includes(activeQuery), [pathname]);
  const { isOpen, onToggle } = useDisclosure();

  const isSubMenuActive = useCallback((href) => pathname === href, [pathname]);

  useEffect(() => {
    if (!isOpen && isActive) onToggle();
  }, []);

  return (
    <MenuCollapseContext.Provider
      value={{ isOpen, isActive, isSubMenuActive, onToggle }}
    >
      <VStack alignItems="stretch" spacing="0">
        {children}
      </VStack>
    </MenuCollapseContext.Provider>
  );
}

function MenuCollapseButton({ children, icon }) {
  const { isActive, onToggle } = useMenuCollapse();

  return (
    <Button
      leftIcon={icon ? <Icon as={icon} /> : null}
      variant="ghost"
      justifyContent="start"
      cursor="pointer"
      bgColor={isActive ? 'gray.600' : null}
      color={isActive ? 'white' : null}
      onClick={onToggle}
      _hover={{ bgColor: isActive ? 'gray.700' : 'inherit' }}
    >
      {children}
    </Button>
  );
}

function MenuCollapseSubMenu({ children, href }) {
  const { isSubMenuActive } = useMenuCollapse();
  const isActive = isSubMenuActive(href);

  return (
    <Link href={href} passHref legacyBehavior>
      <Button
        as="a"
        variant="ghost"
        justifyContent="start"
        bgColor={isActive ? 'gray.600' : null}
        color={isActive ? 'white' : null}
        _hover={{ bgColor: isActive ? 'gray.700' : 'inherit' }}
      >
        {children}
      </Button>
    </Link>
  );
}

function MenuCollapseContent({ children }) {
  const { isOpen } = useMenuCollapse();

  return (
    <Collapse in={isOpen}>
      <VStack alignItems="stretch" pl="6" pt="2">
        {children}
      </VStack>
    </Collapse>
  );
}
