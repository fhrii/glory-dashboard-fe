import { Box, Flex, VStack } from '@chakra-ui/react';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { useCallback, useEffect } from 'react';

import { useBoundStore } from '@/store';

import { DashboardLayoutNavbar } from './DashboardLayoutNavbar';

const DashboardLayoutSidebar = dynamic(() =>
  import('./DashboardLayoutSidebar').then((mod) => mod.DashboardLayoutSidebar)
);

export function DashboardLayout({ children }) {
  const router = useRouter();
  const isAuthenticated = useBoundStore(
    useCallback((state) => state.isAuthenticated, [])
  );

  useEffect(() => {
    if (!isAuthenticated) router.push('/');
  }, [isAuthenticated]);

  return (
    <Box bg="gray.50" minHeight="100vh">
      <Flex minHeight="100vh">
        <DashboardLayoutSidebar />
        <VStack
          position="relative"
          align="stretch"
          flexGrow="1"
          spacing="0"
          px="6"
          overflow="hidden"
        >
          <Box
            position="absolute"
            top="0"
            left="0"
            width="full"
            height="full"
            bgImage='url("/img/bg-main.jpeg")'
            bgRepeat="no-repeat"
            bgSize="cover"
            bgPosition="center 40%"
            filter="auto"
            blur="sm"
          />
          <VStack paddingY="4" spacing="4" alignItems="stretch">
            <DashboardLayoutNavbar />
            <Box as="main" zIndex="1">
              {children}
            </Box>
          </VStack>
        </VStack>
      </Flex>
    </Box>
  );
}
