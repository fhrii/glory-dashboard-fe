import { Box } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { useCallback, useEffect } from 'react';

import { useBoundStore } from '@/store';

export function AuthLayout({ children }) {
  const router = useRouter();
  const isAuthenticated = useBoundStore(
    useCallback((state) => state.isAuthenticated, [])
  );

  useEffect(() => {
    if (isAuthenticated) router.push('/dashboard');
  }, [isAuthenticated]);

  return (
    <Box
      bg="gray.50"
      minHeight="100vh"
      position="relative"
      align="stretch"
      flexGrow="1"
      spacing="0"
      overflow="hidden"
    >
      <Box
        position="absolute"
        top="0"
        left="0"
        width="full"
        height="full"
        bgImage='url("/img/bg-main.jpeg")'
        bgRepeat="no-repeat"
        bgSize="cover"
        bgPosition="center 40%"
        filter="auto"
        blur="sm"
      />
      <Box as="main" position="relative" zIndex="1" minHeight="100vh">
        {children}
      </Box>
    </Box>
  );
}
