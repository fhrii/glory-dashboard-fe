import { createStandaloneToast } from '@chakra-ui/react';

export const { ToastContainer, toast } = createStandaloneToast({
  defaultOptions: { isClosable: true, position: 'top-right' },
});
