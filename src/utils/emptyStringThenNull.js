export function emptyStringThenNull(value) {
  if (value === '') return null;
  return value;
}
