import Axios, { AxiosError } from 'axios';
import jwt from 'jsonwebtoken';

import { RequestError } from '@/exceptions';
import { useBoundStore } from '@/store';
import { toast } from '@/utils/toast';

export const axios = Axios.create({
  baseURL: process.env.NEXT_PUBLIC_BACKEND_HOST,
});

axios.interceptors.request.use((config) => {
  const requestConfig = config;
  const { accessToken, removeAccessToken } = useBoundStore.getState();

  if ((accessToken, removeAccessToken)) {
    const tokenPayload = jwt.decode(accessToken);

    if (!tokenPayload) {
      removeAccessToken();
      return requestConfig;
    }

    const now = Date.now();
    const expiredTime = tokenPayload.exp * 1000;
    const isExpired = now > expiredTime;

    if (isExpired) {
      removeAccessToken();
      return requestConfig;
    }

    requestConfig.headers.Authorization = `Bearer ${accessToken}`;
  }

  return requestConfig;
});

axios.interceptors.response.use(
  (response) => response.data,
  (error) => {
    const statusCode = error.response?.status ?? 999;
    const message = error.response?.data?.data?.message;
    const isInternalServerError = statusCode >= 500;
    const isAuthenticationError = statusCode === 401;

    if (!isInternalServerError) {
      if (isAuthenticationError) {
        const { removeAccessToken } = useBoundStore.getState();
        toast({
          colorScheme: 'red',
          title: 'Authentication Error',
          description: 'Mohon untuk melakukan login kembali',
        });
        removeAccessToken();
      }

      return Promise.reject(new RequestError(message, statusCode));
    }

    if (error instanceof AxiosError && error.code === 'ERR_NETWORK') {
      toast({
        colorScheme: 'red',
        title: 'Network Error',
        description: 'Terjadi kesalahan. Periksa koneksi Anda',
      });

      return Promise.reject(error);
    }

    toast({
      colorScheme: 'red',
      title: 'Error',
      description: message || 'Unknown Error',
    });

    return Promise.reject(error);
  }
);
