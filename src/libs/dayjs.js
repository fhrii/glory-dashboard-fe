import dayjsDefault from 'dayjs';
import timezone from 'dayjs/plugin/timezone';
import utc from 'dayjs/plugin/utc';

export const dayjs = dayjsDefault;
dayjs.extend(utc);
dayjs.extend(timezone);

dayjs.prototype.formatMonthDate = function formatMonthDate() {
  return this.format('DD MMM');
};

dayjs.prototype.formatYearMonthDate = function formatYearMonthDate() {
  return this.format('YYYY-MM-DD');
};

dayjs.prototype.formatYearMonthNameDate = function formatYearMonthNameDate() {
  return this.format('DD MMMM YYYY');
};
