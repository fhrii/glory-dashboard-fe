import { useCallback, useState } from 'react';

export const useCreatePaginate = () => {
  const take = 10;
  const [skip, setSkip] = useState(0);

  const setPage = useCallback(
    ({ selected }) => {
      if (selected <= 0) {
        setSkip(0);
        return;
      }

      setSkip(take * selected);
    },
    [skip]
  );

  return { skip, take, setPage };
};
