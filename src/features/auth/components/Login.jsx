import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  HStack,
  Input,
  Spacer,
  Text,
  useBoolean,
  VStack,
} from '@chakra-ui/react';
import { useCallback } from 'react';
import zod from 'zod';

import { Form, useForm } from '@/components/Form';
import { RequestError } from '@/exceptions';
import { useBoundStore } from '@/store';
import { toast } from '@/utils/toast';

import { useAuthLogin } from '../services';

const LoginSchemaErrorConst = {
  USERNAME_REQUIRED: 'Username harus diisi',
  PASSWORD_REQUIRED: 'Password harus diisi',
};

const LoginSchema = zod.object({
  username: zod.string().min(1, LoginSchemaErrorConst.USERNAME_REQUIRED),
  password: zod.string().min(1, LoginSchemaErrorConst.PASSWORD_REQUIRED),
});

export function Login({ onSuccess }) {
  const [isLoading, setIsLoading] = useBoolean();
  const methods = useForm(LoginSchema);
  const addAccessToken = useBoundStore(
    useCallback((state) => state.addAccessToken, [])
  );
  const { mutateAsync: login } = useAuthLogin();

  const onSubmit = useCallback(async (data) => {
    try {
      setIsLoading.on();
      const loginData = await login(data);
      addAccessToken(loginData.data.credentials.accessToken);
      onSuccess?.();
    } catch (error) {
      setIsLoading.off();
      if (error instanceof RequestError) {
        toast({
          colorScheme: 'red',
          title: 'Gagal login',
          description: 'Username atau password salah',
        });
      }
    }

    onSuccess?.();
  }, []);

  return (
    <Form withForm={methods} onSubmit={onSubmit}>
      {({ formState, register }) => (
        <VStack alignItems="stretch" spacing="4">
          <VStack>
            <Heading as="div" size="lg">
              Login
            </Heading>
            <Text lineHeight="shorter" color="">
              Presensi Karyawan dan Pendataan Tanaman Porang
            </Text>
          </VStack>
          <VStack alignItems="stretch">
            <FormControl isInvalid={!!formState.errors.username}>
              <FormLabel>Username</FormLabel>
              <Input {...register('username')} />
              <FormErrorMessage>
                {formState.errors.username?.message}
              </FormErrorMessage>
            </FormControl>
            <FormControl isInvalid={!!formState.errors.password}>
              <FormLabel>Password</FormLabel>
              <Input type="password" {...register('password')} />
              <FormErrorMessage>
                {formState.errors.password?.message}
              </FormErrorMessage>
            </FormControl>
            <HStack>
              <Spacer />
              <Button type="submit" colorScheme="blue" isLoading={isLoading}>
                Login
              </Button>
            </HStack>
          </VStack>
          <HStack>
            <Spacer />
            <Text as="small">PT Glory Argo Indonesia</Text>
            <Spacer />
          </HStack>
        </VStack>
      )}
    </Form>
  );
}
