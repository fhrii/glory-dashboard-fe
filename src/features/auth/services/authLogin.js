import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const authLogin = (data) => axios.post('/auth/login', data);

export const useAuthLogin = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries();
    },
    mutationFn: authLogin,
    ...config,
  });
