import { Heading, VStack } from '@chakra-ui/react';
import dynamic from 'next/dynamic';
import { useMemo } from 'react';

import { dayjs } from '@/libs/dayjs';

import { useGetPlantStatistic } from '../services';

const ReactApexChart = dynamic(() => import('react-apexcharts'), {
  ssr: false,
});

export function PlantLineChart({ label }) {
  const today = useMemo(
    () => dayjs().set('hour', 7).set('minute', 0).set('second', 0),
    []
  );
  const categories = useMemo(
    () => [
      today.subtract(6, 'day'),
      today.subtract(5, 'day'),
      today.subtract(4, 'day'),
      today.subtract(3, 'day'),
      today.subtract(2, 'day'),
      today.subtract(1, 'day'),
      today,
    ],
    []
  );
  const { data: plantStatisticData } = useGetPlantStatistic();
  const plantSeries = useMemo(() => {
    if (!plantStatisticData) return [];
    const plantStatistic = plantStatisticData.data.statistic.flatMap(
      (plantStats) => plantStats.seeds
    );
    const plantStatisticSeeds = plantStatistic
      .filter(
        (plantStat, index) =>
          plantStatistic.findIndex(
            (aPlantStat) => aPlantStat.seed.id === plantStat.seed.id
          ) === index
      )
      .map((plantStat) => plantStat.seed);
    return plantStatisticSeeds.map((seed) => ({
      name: seed.name,
      data: categories.map((category) => {
        const categoryStatistic = plantStatisticData.data.statistic.find(
          (plantStat) => dayjs(plantStat.date).unix() === category.unix()
        );

        if (!categoryStatistic) return 0;

        const categorySeedStatistic = categoryStatistic.seeds.find(
          (plantStat) => plantStat.seed.id === seed.id
        );

        if (!categorySeedStatistic) return 0;

        return categorySeedStatistic.plant;
      }),
    }));
  }, [plantStatisticData]);
  const options = useMemo(
    () => ({
      chart: {
        height: 300,
        type: 'line',
        toolbar: {
          show: false,
        },
        zoom: {
          enabled: false,
        },
        animations: {
          enabled: true,
        },
      },
      stroke: {
        width: [5, 5, 5, 5, 5, 5],
        curve: 'smooth',
      },
      yaxis: [
        {
          labels: {
            formatter(val) {
              return val.toFixed(0);
            },
          },
        },
      ],
      xaxis: {
        categories: categories.map((category) => category.formatMonthDate()),
      },
    }),
    []
  );

  return (
    <VStack
      alignItems="stretch"
      padding="4"
      bgColor="white"
      rounded="lg"
      border="1px"
      borderColor="gray.200"
      overflow="hidden"
      spacing="6"
    >
      <Heading as="div" size="md">
        {label || '7 Hari Terakhir'}
      </Heading>
      <ReactApexChart
        options={options}
        series={plantSeries}
        type="line"
        height={300}
      />
    </VStack>
  );
}
