import { Heading, VStack } from '@chakra-ui/react';
import dynamic from 'next/dynamic';
import { useMemo } from 'react';

import { dayjs } from '@/libs/dayjs';

import { useGetPlantStatistic } from '../services';

const ReactApexChart = dynamic(() => import('react-apexcharts'), {
  ssr: false,
});

export function PlantPieChart({ label }) {
  const today = useMemo(
    () => dayjs().set('hour', 7).set('minute', 0).set('second', 0),
    []
  );
  const todayDateString = useMemo(() => today.formatYearMonthNameDate(), []);
  const { data: plantStatisticData } = useGetPlantStatistic();
  const todayPlantStatistic = useMemo(() => {
    if (!plantStatisticData) return [];
    return (
      plantStatisticData.data.statistic.find(
        (plantStat) => dayjs(plantStat.date).unix() === today.unix()
      )?.seeds ?? []
    );
  }, [plantStatisticData]);
  const plantSeries = useMemo(
    () => todayPlantStatistic.map((plantStat) => plantStat.plant),
    [todayPlantStatistic]
  );
  const options = useMemo(
    () => ({
      chart: {
        type: 'donut',
      },
      labels: todayPlantStatistic.map((plantStat) => plantStat.seed.name),
      dataLabels: {
        enabled: false,
      },
    }),
    [todayPlantStatistic]
  );

  return (
    <VStack
      alignItems="stretch"
      padding="4"
      bgColor="white"
      rounded="lg"
      border="1px"
      borderColor="gray.200"
      overflow="hidden"
      spacing="6"
    >
      <Heading as="div" size="md">
        {label || todayDateString}
      </Heading>
      <ReactApexChart
        options={options}
        series={plantSeries}
        type="donut"
        height={270}
      />
    </VStack>
  );
}
