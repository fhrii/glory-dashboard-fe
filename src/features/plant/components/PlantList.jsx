import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Box,
  Button,
  ButtonGroup,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  HStack,
  Icon,
  IconButton,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Spacer,
  Spinner,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import { Else, If, Then } from 'react-if';
import zod from 'zod';

import { Form, useForm } from '@/components/Form';
import { Pagination } from '@/components/UI';
import { RequestError } from '@/exceptions';
import { useGetBlocks } from '@/features/block';
import { useGetSeeds } from '@/features/seed';
import { dayjs } from '@/libs/dayjs';
import { toast } from '@/utils/toast';

import {
  useCreatePlant,
  useDeletePlant,
  useGetPlants,
  useUpdatePlant,
} from '../services';

const PlantSchemaErrorConst = {
  SEED_REQUIRED: 'Bibit harus diisi',
  BLOCK_REQUIRED: 'Blok harus diisi',
  PLANT_NUMBER: 'Tanam harus diisi',
  PLANT_MINIMUM: 'Tanam harus lebih dari 0',
  DATE_REQUIRED: 'Tanggal harus diisi',
};

const PlantSchema = zod.object({
  seed: zod.string().min(1, PlantSchemaErrorConst.SEED_REQUIRED),
  block: zod.string().min(1, PlantSchemaErrorConst.BLOCK_REQUIRED),
  plant: zod.preprocess(
    (plant) => parseInt(plant, 10),
    zod
      .number({ invalid_type_error: PlantSchemaErrorConst.PLANT_NUMBER })
      .positive(PlantSchemaErrorConst.PLANT_MINIMUM)
  ),
  date: zod.string().min(1, PlantSchemaErrorConst.DATE_REQUIRED),
});

const defaultPresenceDate = new Date().toISOString().split('T')[0];

const defaultValues = {
  date: defaultPresenceDate,
};

export function PlantList() {
  const methods = useForm(PlantSchema, { defaultValues });
  const [plantSeedFilter, setPlantSeedFilter] = useState();
  const [plantBlockFilter, setPlantBlockFilter] = useState();
  const {
    data: plantsData,
    isLoading: isPlantsDataLoading,
    isError: isPlantsDataError,
    setPage: setPlantsDataPage,
  } = useGetPlants({ seed: plantSeedFilter, block: plantBlockFilter });
  const { data: seedsData } = useGetSeeds();
  const { data: blocksData } = useGetBlocks();
  const { mutateAsync: addPlant, isLoading: isAddPlantLoading } =
    useCreatePlant();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const onPlantSeedFilterChange = useCallback(
    (event) => setPlantSeedFilter(event.target.value),
    []
  );

  const onPlantBlockFilterChange = useCallback(
    (event) => setPlantBlockFilter(event.target.value),
    []
  );

  const onModalClose = useCallback(() => {
    onClose();
    methods.reset();
  }, []);

  const onSubmit = useCallback(async ({ seed, block, plant, date }) => {
    try {
      const data = { seedId: seed, blockId: block, plant, date };
      await addPlant(data);
      onModalClose();
    } catch (error) {
      if (error instanceof RequestError) {
        if (error.statusCode === 400) {
          toast({
            colorScheme: 'red',
            title: 'Gagal menambahkan tanam baru',
            description:
              'Tanam dengan tanggal, bibit, dan blok tersebut sudah ada',
          });
          return;
        }

        toast({
          colorScheme: 'red',
          title: 'Gagal menambahkan tanam baru',
          description: error.message,
        });
      }
    }
  }, []);

  return (
    <VStack
      alignItems="stretch"
      bgColor="white"
      rounded="lg"
      border="1px"
      borderColor="gray.200"
      overflow="hidden"
    >
      <HStack padding="4" borderBottom="1px" borderBottomColor="gray.200">
        <Heading as="h3" size="md">
          Tanam
        </Heading>
        <Spacer />
        <Button colorScheme="blue" onClick={onOpen}>
          Setor Tanam
        </Button>
      </HStack>
      <VStack alignItems="stretch" padding="4" spacing="6">
        <HStack>
          <Box>
            <Select
              variant="filled"
              size="sm"
              rounded="lg"
              placeholder="Bibit"
              value={plantSeedFilter}
              onChange={onPlantSeedFilterChange}
            >
              {seedsData?.data.seeds.map((seed) => (
                <option key={seed.id} value={seed.name}>
                  {seed.name}
                </option>
              ))}
            </Select>
          </Box>
          <Box>
            <Select
              variant="filled"
              size="sm"
              rounded="lg"
              placeholder="Blok"
              value={plantBlockFilter}
              onChange={onPlantBlockFilterChange}
            >
              {blocksData?.data.blocks.map((block) => (
                <option key={block.id} value={block.name}>
                  {block.name}
                </option>
              ))}
            </Select>
          </Box>
        </HStack>
        <TableContainer>
          <Table>
            <Thead>
              <Tr>
                <Th>Bibit</Th>
                <Th>Blok</Th>
                <Th>Tanam</Th>
                <Th>Tanggal</Th>
                <Th width="0">Aksi</Th>
              </Tr>
            </Thead>
            <Tbody>
              <If condition={!isPlantsDataLoading}>
                <Then>
                  <If condition={!isPlantsDataError}>
                    <Then>
                      <If
                        condition={
                          !!plantsData && plantsData.data.plants.length > 0
                        }
                      >
                        <Then>
                          {plantsData?.data.plants.map((plant) => (
                            <PlantListTableRow key={plant.id} {...plant} />
                          ))}
                        </Then>
                        <Else>
                          <Tr>
                            <Td colSpan="5" textAlign="center">
                              <Box padding="4" textColor="gray.600">
                                Tidak ada tanam
                              </Box>
                            </Td>
                          </Tr>
                        </Else>
                      </If>
                    </Then>
                    <Else>
                      <Tr>
                        <Td colSpan="5" textAlign="center">
                          <Box padding="4">
                            Terjadi kesalahan. Silahkan refresh browser.
                          </Box>
                        </Td>
                      </Tr>
                    </Else>
                  </If>
                </Then>
                <Else>
                  <Tr>
                    <Td colSpan="5">
                      <HStack padding="4">
                        <Spacer />
                        <Spinner />
                        <Spacer />
                      </HStack>
                    </Td>
                  </Tr>
                </Else>
              </If>
            </Tbody>
          </Table>
        </TableContainer>
        <HStack>
          <Spacer />
          <Pagination
            onPageChange={setPlantsDataPage}
            totalPages={plantsData?.data.totalPages}
          />
        </HStack>
      </VStack>
      <Modal
        size="2xl"
        blockScrollOnMount={false}
        isOpen={isOpen}
        onClose={onModalClose}
        closeOnOverlayClick={!isAddPlantLoading}
      >
        <ModalOverlay />
        <ModalContent>
          <Form withForm={methods} onSubmit={onSubmit}>
            {({ formState, register }) => (
              <>
                <ModalHeader>Setor Tanam</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <VStack alignItems="stretch">
                    <FormControl isInvalid={!!formState.errors.name}>
                      <FormLabel>Bibit</FormLabel>
                      <Select {...register('seed')}>
                        {seedsData?.data.seeds
                          .filter((seed) => seed.status === 'ACTIVE')
                          .map((seed) => (
                            <option key={seed.id} value={seed.id}>
                              {seed.name}
                            </option>
                          ))}
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.seed?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.block}>
                      <FormLabel>Blok</FormLabel>
                      <Select {...register('block')}>
                        {blocksData?.data.blocks
                          .filter((block) => block.status === 'ACTIVE')
                          .map((block) => (
                            <option key={block.id} value={block.id}>
                              {block.name}
                            </option>
                          ))}
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.block?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.plant}>
                      <FormLabel>Tanam</FormLabel>
                      <Input type="number" {...register('plant')} />
                      <FormErrorMessage>
                        {formState.errors.plant?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.date}>
                      <FormLabel>Tanggal</FormLabel>
                      <Input type="date" {...register('date')} />
                      <FormErrorMessage>
                        {formState.errors.date?.message}
                      </FormErrorMessage>
                    </FormControl>
                  </VStack>
                </ModalBody>
                <ModalFooter>
                  <ButtonGroup>
                    <Button
                      type="button"
                      onClick={onModalClose}
                      isLoading={isAddPlantLoading}
                    >
                      Batal
                    </Button>
                    <Button
                      type="submit"
                      colorScheme="blue"
                      isLoading={isAddPlantLoading}
                    >
                      Setor
                    </Button>
                  </ButtonGroup>
                </ModalFooter>
              </>
            )}
          </Form>
        </ModalContent>
      </Modal>
    </VStack>
  );
}

function PlantListTableRow({ id, seed, block, plant, date }) {
  const parsedDate = useMemo(() => dayjs(date).formatYearMonthDate(), [date]);
  const plantDate = useMemo(
    () => dayjs(date).formatYearMonthNameDate(),
    [date]
  );
  const methods = useForm(PlantSchema, {
    defaultValues: { seed: seed.id, block: block.id, plant, date: parsedDate },
  });
  const { data: seedsData } = useGetSeeds({ status: 1 });
  const { data: blocksData } = useGetBlocks({ status: 1 });
  const { mutateAsync: editPlant, isLoading: isEditPlantLoading } =
    useUpdatePlant();
  const { mutateAsync: deletePlant, isLoading: isDeletePlantLoading } =
    useDeletePlant();
  const {
    isOpen: isAlertOpen,
    onOpen: onAlertOpen,
    onClose: onAlertClose,
  } = useDisclosure();
  const {
    isOpen: isModalOpen,
    onOpen: onModalOpen,
    onClose: onModalClose,
  } = useDisclosure();
  const alertRef = useRef();

  const onSubmit = useCallback(async (data) => {
    try {
      const requestData = {
        id,
        seedId: data.seed,
        blockId: data.block,
        plant: data.plant,
        date: data.date,
      };
      await editPlant(requestData);
      onModalClose();
    } catch (error) {
      if (error instanceof RequestError) {
        toast({
          colorScheme: 'red',
          title: 'Gagal mengubah tanam',
          description: error.message,
        });
      }
    }
  }, []);

  const onDelete = useCallback(async () => {
    try {
      await deletePlant({ id });
      onAlertClose();
    } catch (error) {
      if (error instanceof RequestError) {
        toast({
          colorScheme: 'red',
          title: 'Gagal menghapus tanam',
          description: error.message,
        });
      }
    }
  }, []);

  useEffect(() => {
    methods.reset({
      seed: seed.id,
      block: block.id,
      plant,
      date: parsedDate,
    });
  }, [seed, block, plant, parsedDate]);

  return (
    <Tr>
      <Td>{seed.name}</Td>
      <Td>{block.name}</Td>
      <Td>{plant} Bibit</Td>
      <Td>{plantDate}</Td>
      <Td width="0">
        <ButtonGroup>
          <IconButton
            icon={<Icon as={AiOutlineEdit} />}
            colorScheme="yellow"
            onClick={onModalOpen}
            aria-label={`Ubah ${seed.name}`}
          />
          <IconButton
            icon={<Icon as={AiOutlineDelete} />}
            colorScheme="red"
            onClick={onAlertOpen}
            aria-label={`Hapus ${seed.name}`}
          />
        </ButtonGroup>
      </Td>
      <AlertDialog
        isOpen={isAlertOpen}
        leastDestructiveRef={alertRef}
        onClose={onAlertClose}
        closeOnOverlayClick={!isDeletePlantLoading}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Hapus Setor Tanam
            </AlertDialogHeader>
            <AlertDialogBody>
              Setor Tanam{' '}
              <Text as="strong">
                {seed.name} di blok {block.name} pada {plantDate}
              </Text>{' '}
              akan dihapus dan tidak dapat dikembalikan.
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button ref={alertRef} onClick={onAlertClose}>
                Batal
              </Button>
              <Button colorScheme="red" onClick={onDelete} ml={3}>
                Hapus
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
      <Modal
        size="2xl"
        blockScrollOnMount={false}
        isOpen={isModalOpen}
        onClose={onModalClose}
        closeOnOverlayClick={!isEditPlantLoading}
      >
        <ModalOverlay />
        <ModalContent>
          <Form withForm={methods} onSubmit={onSubmit}>
            {({ formState, register }) => (
              <>
                <ModalHeader>Ubah Setor Tanam</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <VStack alignItems="stretch">
                    <FormControl isInvalid={!!formState.errors.seed}>
                      <FormLabel>Bibit</FormLabel>
                      <Select {...register('seed')}>
                        {seedsData?.data.seeds.map((s) => (
                          <option key={s.id} value={s.id}>
                            {s.name}
                          </option>
                        ))}
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.seed?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.block}>
                      <FormLabel>Blok</FormLabel>
                      <Select {...register('block')}>
                        {blocksData?.data.blocks.map((b) => (
                          <option key={b.id} value={b.id}>
                            {b.name}
                          </option>
                        ))}
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.block?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.plant}>
                      <FormLabel>Tanam</FormLabel>
                      <Input type="number" {...register('plant')} />
                      <FormErrorMessage>
                        {formState.errors.plant?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.date}>
                      <FormLabel>Tanggal</FormLabel>
                      <Input type="date" {...register('date')} />
                      <FormErrorMessage>
                        {formState.errors.date?.message}
                      </FormErrorMessage>
                    </FormControl>
                  </VStack>
                </ModalBody>
                <ModalFooter>
                  <ButtonGroup>
                    <Button
                      type="button"
                      onClick={onModalClose}
                      isLoading={isEditPlantLoading}
                    >
                      Batal
                    </Button>
                    <Button
                      type="submit"
                      colorScheme="blue"
                      isLoading={isEditPlantLoading}
                    >
                      Ubah
                    </Button>
                  </ButtonGroup>
                </ModalFooter>
              </>
            )}
          </Form>
        </ModalContent>
      </Modal>
    </Tr>
  );
}
