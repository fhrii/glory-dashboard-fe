import { useQuery } from 'react-query';

import { useCreatePaginate } from '@/hooks';
import { axios } from '@/libs/axios';
import { emptyStringThenNull } from '@/utils/emptyStringThenNull';

export const getPlants = ({ seed, block, skip, take }) =>
  axios.get('/plants', { params: { seed, block, skip, take } });

export const useGetPlants = ({
  seed: oldSeed,
  block: oldBlock,
  config,
} = {}) => {
  const { skip, take, setPage } = useCreatePaginate();
  const seed = emptyStringThenNull(oldSeed);
  const block = emptyStringThenNull(oldBlock);

  return {
    setPage,
    ...useQuery({
      queryKey: ['plants', { seed, block, skip, take }],
      queryFn: () => getPlants({ seed, block, skip, take }),
      ...config,
    }),
  };
};
