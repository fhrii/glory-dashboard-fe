import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const deletePlant = ({ id }) => axios.delete(`/plants/${id}`);

export const useDeletePlant = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['plants']);
      queryClient.invalidateQueries(['plantStatistic']);
    },
    mutationFn: deletePlant,
    ...config,
  });
