import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const updatePlant = ({ id, ...data }) =>
  axios.put(`/plants/${id}`, data);

export const useUpdatePlant = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['plants']);
      queryClient.invalidateQueries(['plantStatistic']);
    },
    mutationFn: updatePlant,
    ...config,
  });
