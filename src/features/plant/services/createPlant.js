import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const createPlant = (data) => axios.post('/plants', data);

export const useCreatePlant = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['plants']);
      queryClient.invalidateQueries(['plantStatistic']);
    },
    mutationFn: createPlant,
    ...config,
  });
