import { useQuery } from 'react-query';

import { axios } from '@/libs/axios';

export const getPlantStatistic = () => axios.get('/plants/statistic');

export const useGetPlantStatistic = ({ config } = {}) =>
  useQuery({
    queryKey: ['plantStatistic'],
    queryFn: () => getPlantStatistic(),
    ...config,
  });
