import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Box,
  Button,
  ButtonGroup,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  HStack,
  Icon,
  IconButton,
  Input,
  InputGroup,
  InputLeftElement,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Spacer,
  Spinner,
  Table,
  TableContainer,
  Tag,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import {
  AiOutlineDelete,
  AiOutlineEdit,
  AiOutlineSearch,
} from 'react-icons/ai';
import { Else, If, Then } from 'react-if';
import { useDebounce } from 'use-debounce';
import zod from 'zod';

import { Form, useForm } from '@/components/Form';
import { Pagination } from '@/components/UI';
import { RequestError } from '@/exceptions';
import { toast } from '@/utils/toast';

import {
  useCreateBlock,
  useDeleteBlock,
  useGetBlocks,
  useUpdateBlock,
} from '../services';

const BlockSchemaErrorConst = {
  NAME_REQUIRED: 'Nama harus diisi',
  STATUS_ENUM: 'Status harus diantara Aktif atau Tidak Aktif',
};

const BlockSchema = zod.object({
  name: zod.string().min(1, BlockSchemaErrorConst.NAME_REQUIRED),
  status: zod.enum(['ACTIVE', 'NOT_ACTIVE'], BlockSchemaErrorConst.STATUS_ENUM),
});

const defaultValues = {
  status: 'ACTIVE',
};

export function BlockList() {
  const methods = useForm(BlockSchema, { defaultValues });
  const [blockNameFilter, setBlockNameFilter] = useState();
  const [blockStatusFilter, setBlockStatusFilter] = useState();
  const [debouncedBlockNameFilter] = useDebounce(blockNameFilter, 500);
  const {
    data: blocksData,
    isLoading: isBlocksDataLoading,
    isError: isBlocksDataError,
    setPage: setBlockDataPage,
  } = useGetBlocks({
    name: debouncedBlockNameFilter,
    status: blockStatusFilter,
  });
  const { mutateAsync: addBlock, isLoading: isAddBlockLoading } =
    useCreateBlock();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const onBlockNameFilterChange = useCallback(
    (event) => setBlockNameFilter(event.target.value),
    []
  );

  const onBlockStatusFilterChange = useCallback(
    (event) => setBlockStatusFilter(event.target.value),
    []
  );

  const onModalClose = useCallback(() => {
    onClose();
    methods.reset();
  }, []);

  const onSubmit = useCallback(async (data) => {
    try {
      await addBlock(data);
      onModalClose();
    } catch (error) {
      if (error instanceof RequestError) {
        let { message } = error;

        if (message.includes('already exist'))
          message = `Blok ${data.name} sudah ada`;

        toast({
          colorScheme: 'red',
          title: 'Gagal menambahkan blok baru',
          description: message,
        });
      }
    }
  }, []);

  return (
    <VStack
      alignItems="stretch"
      bgColor="white"
      rounded="lg"
      border="1px"
      borderColor="gray.200"
      overflow="hidden"
    >
      <HStack padding="4" borderBottom="1px" borderBottomColor="gray.200">
        <Heading as="h3" size="md">
          Blok
        </Heading>
        <Spacer />
        <Button colorScheme="blue" onClick={onOpen}>
          Tambah Blok
        </Button>
      </HStack>
      <VStack alignItems="stretch" padding="4" spacing="6">
        <HStack>
          <Box>
            <InputGroup size="sm">
              <InputLeftElement pointerEvents="none">
                <Icon as={AiOutlineSearch} />
              </InputLeftElement>
              <Input
                variant="filled"
                placeholder="Cari Blok"
                rounded="lg"
                value={blockNameFilter}
                onChange={onBlockNameFilterChange}
              />
            </InputGroup>
          </Box>
          <Box>
            <Select
              variant="filled"
              size="sm"
              rounded="lg"
              placeholder="Status"
              value={blockStatusFilter}
              onChange={onBlockStatusFilterChange}
            >
              <option value="0">Tidak Aktif</option>
              <option value="1">Aktif</option>
            </Select>
          </Box>
        </HStack>
        <TableContainer>
          <Table>
            <Thead>
              <Tr>
                <Th>Nama</Th>
                <Th>Status</Th>
                <Th width="0">Aksi</Th>
              </Tr>
            </Thead>
            <Tbody>
              <If condition={!isBlocksDataLoading}>
                <Then>
                  <If condition={!isBlocksDataError}>
                    <Then>
                      <If
                        condition={
                          !!blocksData && blocksData.data.blocks.length > 0
                        }
                      >
                        <Then>
                          {blocksData?.data.blocks.map((block) => (
                            <BlockListTableRow key={block.id} {...block} />
                          ))}
                        </Then>
                        <Else>
                          <Tr>
                            <Td colSpan="3" textAlign="center">
                              <Box padding="4" textColor="gray.600">
                                Tidak ada blok
                              </Box>
                            </Td>
                          </Tr>
                        </Else>
                      </If>
                    </Then>
                    <Else>
                      <Tr>
                        <Td colSpan="3" textAlign="center">
                          <Box padding="4">
                            Terjadi kesalahan. Silahkan refresh browser.
                          </Box>
                        </Td>
                      </Tr>
                    </Else>
                  </If>
                </Then>
                <Else>
                  <Tr>
                    <Td colSpan="3">
                      <HStack padding="4">
                        <Spacer />
                        <Spinner />
                        <Spacer />
                      </HStack>
                    </Td>
                  </Tr>
                </Else>
              </If>
            </Tbody>
          </Table>
        </TableContainer>
        <HStack>
          <Spacer />
          <Pagination
            onPageChange={setBlockDataPage}
            totalPages={blocksData?.data.totalPages}
          />
        </HStack>
      </VStack>
      <Modal
        size="2xl"
        blockScrollOnMount={false}
        isOpen={isOpen}
        onClose={onModalClose}
        closeOnOverlayClick={!isAddBlockLoading}
      >
        <ModalOverlay />
        <ModalContent>
          <Form withForm={methods} onSubmit={onSubmit}>
            {({ formState, register }) => (
              <>
                <ModalHeader>Tambah Blok</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <VStack alignItems="stretch">
                    <FormControl isInvalid={!!formState.errors.name}>
                      <FormLabel>Nama</FormLabel>
                      <Input {...register('name')} />
                      <FormErrorMessage>
                        {formState.errors.name?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.status}>
                      <FormLabel>Status</FormLabel>
                      <Select {...register('status')}>
                        <option value="ACTIVE">Aktif</option>
                        <option value="NOT_ACTIVE">Tidak Aktif</option>
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.name?.message}
                      </FormErrorMessage>
                    </FormControl>
                  </VStack>
                </ModalBody>
                <ModalFooter>
                  <ButtonGroup>
                    <Button
                      type="button"
                      onClick={onModalClose}
                      isLoading={isAddBlockLoading}
                    >
                      Batal
                    </Button>
                    <Button
                      type="submit"
                      colorScheme="blue"
                      isLoading={isAddBlockLoading}
                    >
                      Tambah
                    </Button>
                  </ButtonGroup>
                </ModalFooter>
              </>
            )}
          </Form>
        </ModalContent>
      </Modal>
    </VStack>
  );
}

function BlockListTableRow({ id, name, status }) {
  const methods = useForm(BlockSchema, { defaultValues: { name, status } });
  const { mutateAsync: editBlock, isLoading: isEditBlockLoading } =
    useUpdateBlock();
  const { mutateAsync: deleteBlock, isLoading: isDeleteBlockLoading } =
    useDeleteBlock();
  const {
    isOpen: isAlertOpen,
    onOpen: onAlertOpen,
    onClose: onAlertClose,
  } = useDisclosure();
  const {
    isOpen: isModalOpen,
    onOpen: onModalOpen,
    onClose: onModalClose,
  } = useDisclosure();
  const alertRef = useRef();
  const statusColor = useMemo(
    () => (status === 'ACTIVE' ? 'green' : 'red'),
    [status]
  );
  const statusName = useMemo(
    () => (status === 'ACTIVE' ? 'Aktif' : 'Tidak Aktif'),
    [status]
  );

  const onSubmit = useCallback(async (data) => {
    try {
      await editBlock({ id, ...data });
      onModalClose();
    } catch (error) {
      if (error instanceof RequestError) {
        let { message } = error;

        if (message.includes('already exist'))
          message = `Blok ${data.name} sudah ada`;

        toast({
          colorScheme: 'red',
          title: 'Gagal mengubah blok',
          description: message,
        });
      }
    }
  }, []);

  const onDelete = useCallback(async () => {
    try {
      await deleteBlock({ id });
      onAlertClose();
    } catch (error) {
      if (error instanceof RequestError) {
        toast({
          colorScheme: 'red',
          title: 'Gagal menghapus blok',
          description: error.message,
        });
      }
    }
  }, []);

  useEffect(() => {
    methods.reset({ name, status });
  }, [name, status]);

  return (
    <Tr>
      <Td>{name}</Td>
      <Td>
        <Tag colorScheme={statusColor}>{statusName}</Tag>
      </Td>
      <Td width="0">
        <ButtonGroup>
          <IconButton
            icon={<Icon as={AiOutlineEdit} />}
            colorScheme="yellow"
            onClick={onModalOpen}
            aria-label={`Ubah ${name}`}
          />
          <IconButton
            icon={<Icon as={AiOutlineDelete} />}
            colorScheme="red"
            onClick={onAlertOpen}
            aria-label={`Hapus ${name}`}
          />
        </ButtonGroup>
      </Td>
      <AlertDialog
        isOpen={isAlertOpen}
        leastDestructiveRef={alertRef}
        onClose={onAlertClose}
        closeOnOverlayClick={!isDeleteBlockLoading}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Hapus Blok
            </AlertDialogHeader>
            <AlertDialogBody>
              Blok <Text as="strong">{name}</Text> akan dihapus dan tidak dapat
              dikembalikan.
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button
                ref={alertRef}
                onClick={onAlertClose}
                isLoading={isDeleteBlockLoading}
              >
                Batal
              </Button>
              <Button
                colorScheme="red"
                onClick={onDelete}
                ml={3}
                isLoading={isDeleteBlockLoading}
              >
                Hapus
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
      <Modal
        size="2xl"
        blockScrollOnMount={false}
        isOpen={isModalOpen}
        onClose={onModalClose}
        closeOnOverlayClick={!isEditBlockLoading}
      >
        <ModalOverlay />
        <ModalContent>
          <Form withForm={methods} onSubmit={onSubmit}>
            {({ formState, register }) => (
              <>
                <ModalHeader>Ubah Blok</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <VStack alignItems="stretch">
                    <FormControl isInvalid={!!formState.errors.name}>
                      <FormLabel>Nama</FormLabel>
                      <Input {...register('name')} />
                      <FormErrorMessage>
                        {formState.errors.name?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.status}>
                      <FormLabel>Status</FormLabel>
                      <Select {...register('status')}>
                        <option value="ACTIVE">Aktif</option>
                        <option value="NOT_ACTIVE">Tidak Aktif</option>
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.name?.message}
                      </FormErrorMessage>
                    </FormControl>
                  </VStack>
                </ModalBody>
                <ModalFooter>
                  <ButtonGroup>
                    <Button
                      type="button"
                      onClick={onModalClose}
                      isLoading={isEditBlockLoading}
                    >
                      Batal
                    </Button>
                    <Button type="submit" colorScheme="blue">
                      Ubah
                    </Button>
                  </ButtonGroup>
                </ModalFooter>
              </>
            )}
          </Form>
        </ModalContent>
      </Modal>
    </Tr>
  );
}
