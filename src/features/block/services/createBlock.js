import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const createBlock = (data) => axios.post('/blocks', data);

export const useCreateBlock = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['blocks']);
      queryClient.invalidateQueries(['plantStatistic']);
      queryClient.invalidateQueries(['harvestStatistic']);
    },
    mutationFn: createBlock,
    ...config,
  });
