import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const deleteBlock = ({ id }) => axios.delete(`/blocks/${id}`);

export const useDeleteBlock = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['blocks']);
      queryClient.invalidateQueries(['plantStatistic']);
      queryClient.invalidateQueries(['harvestStatistic']);
    },
    mutationFn: deleteBlock,
    ...config,
  });
