import { useQuery } from 'react-query';

import { useCreatePaginate } from '@/hooks';
import { axios } from '@/libs/axios';
import { emptyStringThenNull } from '@/utils/emptyStringThenNull';

export const getBlocks = ({ status, name, skip, take }) =>
  axios.get('/blocks', { params: { status, name, skip, take } });

export const useGetBlocks = ({
  status: oldStatus,
  name: oldName,
  config,
} = {}) => {
  const { skip, take, setPage } = useCreatePaginate();
  const status = emptyStringThenNull(oldStatus);
  const name = emptyStringThenNull(oldName);

  return {
    setPage,
    ...useQuery({
      queryKey: ['blocks', { status, name, skip, take }],
      queryFn: () => getBlocks({ status, name, skip, take }),
      ...config,
    }),
  };
};
