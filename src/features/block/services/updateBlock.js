import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const updateBlock = ({ id, ...data }) =>
  axios.put(`/blocks/${id}`, data);

export const useUpdateBlock = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['blocks']);
      queryClient.invalidateQueries(['plantStatistic']);
      queryClient.invalidateQueries(['harvestStatistic']);
    },
    mutationFn: updateBlock,
    ...config,
  });
