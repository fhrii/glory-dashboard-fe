import { useQuery } from 'react-query';

import { axios } from '@/libs/axios';

export const getHarvestStatistic = () => axios.get('/harvests/statistic');

export const useGetHarvestStatistic = ({ config } = {}) =>
  useQuery({
    queryKey: ['harvestStatistic'],
    queryFn: () => getHarvestStatistic(),
    ...config,
  });
