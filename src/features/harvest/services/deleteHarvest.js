import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const deleteHarvest = ({ id }) => axios.delete(`/harvests/${id}`);

export const useDeleteHarvest = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['harvests']);
      queryClient.invalidateQueries(['harvestStatistic']);
    },
    mutationFn: deleteHarvest,
    ...config,
  });
