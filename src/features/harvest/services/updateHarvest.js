import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const updateHarvest = ({ id, ...data }) =>
  axios.put(`/harvests/${id}`, data);

export const useUpdateHarvest = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['harvests']);
      queryClient.invalidateQueries(['harvestStatistic']);
    },
    mutationFn: updateHarvest,
    ...config,
  });
