import { useQuery } from 'react-query';

import { useCreatePaginate } from '@/hooks';
import { axios } from '@/libs/axios';
import { emptyStringThenNull } from '@/utils/emptyStringThenNull';

export const getHarvests = ({ seed, block, condition, skip, take }) =>
  axios.get('/harvests', { params: { seed, block, condition, skip, take } });

export const useGetHarvests = ({
  seed: oldSeed,
  block: oldBlock,
  condition: oldCondition,
  config,
} = {}) => {
  const { skip, take, setPage } = useCreatePaginate();
  const seed = emptyStringThenNull(oldSeed);
  const block = emptyStringThenNull(oldBlock);
  const condition = emptyStringThenNull(oldCondition);

  return {
    setPage,
    ...useQuery({
      queryKey: ['harvests', { seed, block, condition, skip, take }],
      queryFn: () => getHarvests({ seed, block, condition, skip, take }),
      ...config,
    }),
  };
};
