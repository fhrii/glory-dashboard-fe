import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const createHarvest = (data) => axios.post('/harvests', data);

export const useCreateHarvest = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['harvests']);
      queryClient.invalidateQueries(['harvestStatistic']);
    },
    mutationFn: createHarvest,
    ...config,
  });
