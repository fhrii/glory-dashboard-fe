import { Heading, VStack } from '@chakra-ui/react';
import dynamic from 'next/dynamic';
import { useMemo } from 'react';

import { dayjs } from '@/libs/dayjs';

import { useGetHarvestStatistic } from '../services';

const ReactApexChart = dynamic(() => import('react-apexcharts'), {
  ssr: false,
});

export function HarvestPieChart({ label }) {
  const today = useMemo(
    () => dayjs().set('hour', 7).set('minute', 0).set('second', 0),
    []
  );
  const todayDateString = useMemo(() => today.formatYearMonthNameDate(), []);
  const { data: harvestStatisticData } = useGetHarvestStatistic();
  const todayHarvestStatistic = useMemo(() => {
    if (!harvestStatisticData) return [];
    return (
      harvestStatisticData.data.statistic.find(
        (harvestStat) => dayjs(harvestStat.date).unix() === today.unix()
      )?.seeds ?? []
    );
  }, [harvestStatisticData]);
  const harvestSeries = useMemo(
    () => todayHarvestStatistic.map((harvestStat) => harvestStat.harvest),
    [todayHarvestStatistic]
  );
  const options = useMemo(
    () => ({
      chart: {
        type: 'donut',
      },
      labels: todayHarvestStatistic.map((harvestStat) => harvestStat.seed.name),
      dataLabels: {
        enabled: false,
      },
    }),
    [todayHarvestStatistic]
  );

  return (
    <VStack
      alignItems="stretch"
      padding="4"
      bgColor="white"
      rounded="lg"
      border="1px"
      borderColor="gray.200"
      overflow="hidden"
      spacing="6"
    >
      <Heading as="div" size="md">
        {label || todayDateString}
      </Heading>
      <ReactApexChart
        options={options}
        series={harvestSeries}
        type="donut"
        height={270}
      />
    </VStack>
  );
}
