import { Heading, VStack } from '@chakra-ui/react';
import dynamic from 'next/dynamic';
import { useMemo } from 'react';

import { dayjs } from '@/libs/dayjs';

import { useGetHarvestStatistic } from '../services';

const ReactApexChart = dynamic(() => import('react-apexcharts'), {
  ssr: false,
});

export function HarvestLineChart({ label }) {
  const today = useMemo(
    () => dayjs().set('hour', 7).set('minute', 0).set('second', 0),
    []
  );
  const categories = useMemo(
    () => [
      today.subtract(6, 'day'),
      today.subtract(5, 'day'),
      today.subtract(4, 'day'),
      today.subtract(3, 'day'),
      today.subtract(2, 'day'),
      today.subtract(1, 'day'),
      today,
    ],
    []
  );
  const { data: harvestStatisticData } = useGetHarvestStatistic();
  const presenceSeries = useMemo(() => {
    if (!harvestStatisticData) return [];
    const harvestStatistic = harvestStatisticData.data.statistic.flatMap(
      (harvestStat) => harvestStat.seeds
    );
    const harvestStatisticSeeds = harvestStatistic
      .filter(
        (harvestStat, index) =>
          harvestStatistic.findIndex(
            (aHarvestStat) => aHarvestStat.seed.id === harvestStat.seed.id
          ) === index
      )
      .map((harvestStat) => harvestStat.seed);
    return harvestStatisticSeeds.map((seed) => ({
      name: seed.name,
      data: categories.map((category) => {
        const categoryStatistic = harvestStatisticData.data.statistic.find(
          (harvestStat) => dayjs(harvestStat.date).unix() === category.unix()
        );

        if (!categoryStatistic) return 0;

        const categorySeedStatistic = categoryStatistic.seeds.find(
          (harvestStat) => harvestStat.seed.id === seed.id
        );

        if (!categorySeedStatistic) return 0;

        return categorySeedStatistic.harvest;
      }),
    }));
  }, [harvestStatisticData]);
  const options = useMemo(
    () => ({
      chart: {
        height: 300,
        type: 'line',
        toolbar: {
          show: false,
        },
        zoom: {
          enabled: false,
        },
        animations: {
          enabled: true,
        },
      },
      stroke: {
        width: [5, 5, 5, 5, 5, 5],
        curve: 'smooth',
      },
      yaxis: [
        {
          labels: {
            formatter(val) {
              return val.toFixed(0);
            },
          },
        },
      ],
      xaxis: {
        categories: categories.map((category) => category.formatMonthDate()),
      },
    }),
    []
  );

  return (
    <VStack
      alignItems="stretch"
      padding="4"
      bgColor="white"
      rounded="lg"
      border="1px"
      borderColor="gray.200"
      overflow="hidden"
      spacing="6"
    >
      <Heading as="div" size="md">
        {label || '7 Hari Terakhir'}
      </Heading>
      <ReactApexChart
        options={options}
        series={presenceSeries}
        type="line"
        height={300}
      />
    </VStack>
  );
}
