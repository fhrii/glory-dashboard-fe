import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Box,
  Button,
  ButtonGroup,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  HStack,
  Icon,
  IconButton,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Spacer,
  Spinner,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { AiOutlineDelete, AiOutlineEdit } from 'react-icons/ai';
import { Else, If, Then } from 'react-if';
import zod from 'zod';

import { Form, useForm } from '@/components/Form';
import { Pagination } from '@/components/UI';
import { RequestError } from '@/exceptions';
import { useGetBlocks } from '@/features/block';
import { useGetConditions } from '@/features/condition';
import { useGetSeeds } from '@/features/seed';
import { dayjs } from '@/libs/dayjs';
import { toast } from '@/utils/toast';

import {
  useCreateHarvest,
  useDeleteHarvest,
  useGetHarvests,
  useUpdateHarvest,
} from '../services';

const HarvestSchemaErrorConst = {
  NAME_REQUIRED: 'Nama harus diisi',
  BLOCK_REQUIRED: 'Blok harus diisi',
  HARVEST_NUMBER: 'Tanam harus diisi',
  HARVEST_MINIMUM: 'Tanam harus lebih dari 0',
  CONDITION_REQUIRED: 'Kondisi harus diisi',
  DATE_REQUIRED: 'Tanggal harus diisi',
};

const HarvestSchema = zod.object({
  seed: zod.string().min(1, HarvestSchemaErrorConst.NAME_REQUIRED),
  block: zod.string().min(1, HarvestSchemaErrorConst.BLOCK_REQUIRED),
  condition: zod.string().min(1, HarvestSchemaErrorConst.CONDITION_REQUIRED),
  harvest: zod.preprocess(
    (harvest) => parseInt(harvest, 10),
    zod
      .number({ invalid_type_error: HarvestSchemaErrorConst.HARVEST_NUMBER })
      .positive(HarvestSchemaErrorConst.HARVEST_MINIMUM)
  ),
  date: zod.string().min(1, HarvestSchemaErrorConst.DATE_REQUIRED),
});

const defaultPresenceDate = new Date().toISOString().split('T')[0];

const defaultValues = {
  date: defaultPresenceDate,
};

export function HarvestList() {
  const methods = useForm(HarvestSchema, { defaultValues });
  const [harvestSeedFilter, setHarvestSeedFilter] = useState();
  const [harvestBlockFilter, setHarvestBlockFilter] = useState();
  const [harvestConditionFilter, setHarvestConditionFilter] = useState();
  const {
    data: harvestsData,
    isLoading: isHarvestsDataLoading,
    isError: isHarvestsDataError,
    setPage: setHarvestsDataPage,
  } = useGetHarvests({
    seed: harvestSeedFilter,
    block: harvestBlockFilter,
    condition: harvestConditionFilter,
  });
  const { data: seedsData } = useGetSeeds();
  const { data: blocksData } = useGetBlocks();
  const { data: conditionsData } = useGetConditions();
  const { mutateAsync: addHarvest, isLoading: isAddHarvestLoading } =
    useCreateHarvest();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const onHarvestSeedFilterChange = useCallback(
    (event) => setHarvestSeedFilter(event.target.value),
    []
  );

  const onHarvestBlockFilterChange = useCallback(
    (event) => setHarvestBlockFilter(event.target.value),
    []
  );

  const onHarvestConditionFilterChange = useCallback(
    (event) => setHarvestConditionFilter(event.target.value),
    []
  );

  const onModalClose = useCallback(() => {
    onClose();
    methods.reset();
  }, []);

  const onSubmit = useCallback(
    async ({ seed, block, condition, harvest, date }) => {
      const data = {
        seedId: seed,
        blockId: block,
        conditionId: condition,
        harvest,
        date,
      };
      try {
        await addHarvest(data);
        onModalClose();
      } catch (error) {
        if (error instanceof RequestError) {
          toast({
            colorScheme: 'red',
            title: 'Gagal menambahkan panen baru',
            description: error.message,
          });
        }
      }
    },
    []
  );

  return (
    <VStack
      alignItems="stretch"
      bgColor="white"
      rounded="lg"
      border="1px"
      borderColor="gray.200"
      overflow="hidden"
    >
      <HStack padding="4" borderBottom="1px" borderBottomColor="gray.200">
        <Heading as="h3" size="md">
          Panen
        </Heading>
        <Spacer />
        <Button colorScheme="blue" onClick={onOpen}>
          Setor Panen
        </Button>
      </HStack>
      <VStack alignItems="stretch" padding="4" spacing="6">
        <HStack>
          <Box>
            <Select
              variant="filled"
              size="sm"
              rounded="lg"
              placeholder="Bibit"
              value={harvestSeedFilter}
              onChange={onHarvestSeedFilterChange}
            >
              {seedsData?.data.seeds.map((seed) => (
                <option key={seed.id} value={seed.name}>
                  {seed.name}
                </option>
              ))}
            </Select>
          </Box>
          <Box>
            <Select
              variant="filled"
              size="sm"
              rounded="lg"
              placeholder="Blok"
              value={harvestBlockFilter}
              onChange={onHarvestBlockFilterChange}
            >
              {blocksData?.data.blocks.map((block) => (
                <option key={block.id} value={block.name}>
                  {block.name}
                </option>
              ))}
            </Select>
          </Box>
          <Box>
            <Select
              variant="filled"
              size="sm"
              rounded="lg"
              placeholder="Kondisi"
              value={harvestConditionFilter}
              onChange={onHarvestConditionFilterChange}
            >
              {conditionsData?.data.conditions.map((condition) => (
                <option key={condition.id} value={condition.name}>
                  {condition.name}
                </option>
              ))}
            </Select>
          </Box>
        </HStack>
        <TableContainer>
          <Table>
            <Thead>
              <Tr>
                <Th>Nama</Th>
                <Th>Blok</Th>
                <Th>Kondisi</Th>
                <Th>Panen</Th>
                <Th>Tanggal</Th>
                <Th width="0">Aksi</Th>
              </Tr>
            </Thead>
            <Tbody>
              <If condition={!isHarvestsDataLoading}>
                <Then>
                  <If condition={!isHarvestsDataError}>
                    <Then>
                      <If
                        condition={
                          !!harvestsData &&
                          harvestsData.data.harvests.length > 0
                        }
                      >
                        <Then>
                          {harvestsData?.data.harvests.map((harvest) => (
                            <HarvestListTableRow
                              key={harvest.id}
                              {...harvest}
                            />
                          ))}
                        </Then>
                        <Else>
                          <Tr>
                            <Td colSpan="6" textAlign="center">
                              <Box padding="4" textColor="gray.600">
                                Tidak ada panen
                              </Box>
                            </Td>
                          </Tr>
                        </Else>
                      </If>
                    </Then>
                    <Else>
                      <Tr>
                        <Td colSpan="6" textAlign="center">
                          <Box padding="4">
                            Terjadi kesalahan. Silahkan refresh browser.
                          </Box>
                        </Td>
                      </Tr>
                    </Else>
                  </If>
                </Then>
                <Else>
                  <Tr>
                    <Td colSpan="6">
                      <HStack padding="4">
                        <Spacer />
                        <Spinner />
                        <Spacer />
                      </HStack>
                    </Td>
                  </Tr>
                </Else>
              </If>
            </Tbody>
          </Table>
        </TableContainer>
        <HStack>
          <Spacer />
          <Pagination
            onPageChange={setHarvestsDataPage}
            totalPages={harvestsData?.data.totalPages}
          />
        </HStack>
      </VStack>
      <Modal
        size="2xl"
        blockScrollOnMount={false}
        isOpen={isOpen}
        onClose={onClose}
        closeOnOverlayClick={!isAddHarvestLoading}
      >
        <ModalOverlay />
        <ModalContent>
          <Form withForm={methods} onSubmit={onSubmit}>
            {({ formState, register }) => (
              <>
                <ModalHeader>Setor Panen</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <VStack alignItems="stretch">
                    <FormControl isInvalid={!!formState.errors.seed}>
                      <FormLabel>Bibit</FormLabel>
                      <Select {...register('seed')}>
                        {seedsData?.data.seeds
                          .filter((seed) => seed.status === 'ACTIVE')
                          .map((seed) => (
                            <option key={seed.id} value={seed.id}>
                              {seed.name}
                            </option>
                          ))}
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.seed?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.block}>
                      <FormLabel>Blok</FormLabel>
                      <Select {...register('block')}>
                        {blocksData?.data.blocks
                          .filter((block) => block.status === 'ACTIVE')
                          .map((block) => (
                            <option key={block.id} value={block.id}>
                              {block.name}
                            </option>
                          ))}
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.block?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.harvest}>
                      <FormLabel>Panen</FormLabel>
                      <Input type="number" {...register('harvest')} />
                      <FormErrorMessage>
                        {formState.errors.harvest?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.condition}>
                      <FormLabel>Kondisi</FormLabel>
                      <Select {...register('condition')}>
                        {conditionsData?.data.conditions
                          .filter((condition) => condition.status === 'ACTIVE')
                          .map((condition) => (
                            <option key={condition.id} value={condition.id}>
                              {condition.name}
                            </option>
                          ))}
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.condition?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.date}>
                      <FormLabel>Tanggal</FormLabel>
                      <Input type="date" {...register('date')} />
                      <FormErrorMessage>
                        {formState.errors.date?.message}
                      </FormErrorMessage>
                    </FormControl>
                  </VStack>
                </ModalBody>
                <ModalFooter>
                  <ButtonGroup>
                    <Button
                      type="button"
                      onClick={onClose}
                      isLoading={isAddHarvestLoading}
                    >
                      Batal
                    </Button>
                    <Button
                      type="submit"
                      colorScheme="blue"
                      isLoading={isAddHarvestLoading}
                    >
                      Setor
                    </Button>
                  </ButtonGroup>
                </ModalFooter>
              </>
            )}
          </Form>
        </ModalContent>
      </Modal>
    </VStack>
  );
}

function HarvestListTableRow({ id, seed, block, harvest, condition, date }) {
  const parsedDate = useMemo(() => dayjs(date).formatYearMonthDate(), [date]);
  const harvestDate = useMemo(
    () => dayjs(date).formatYearMonthNameDate(),
    [date]
  );
  const methods = useForm(HarvestSchema, {
    defaultValues: {
      seed: seed.id,
      block: block.id,
      harvest,
      condition: condition.id,
      date: parsedDate,
    },
  });
  const { data: seedsData } = useGetSeeds({ status: 1 });
  const { data: blocksData } = useGetBlocks({ status: 1 });
  const { data: conditionsData } = useGetConditions({ status: 1 });
  const { mutateAsync: editHarvest, isLoading: isEditHarvestLoading } =
    useUpdateHarvest();
  const { mutateAsync: deleteHarvest, isLoading: isDeleteHarvestLoading } =
    useDeleteHarvest();
  const {
    isOpen: isAlertOpen,
    onOpen: onAlertOpen,
    onClose: onAlertClose,
  } = useDisclosure();
  const {
    isOpen: isModalOpen,
    onOpen: onModalOpen,
    onClose: onModalClose,
  } = useDisclosure();
  const alertRef = useRef();

  const onSubmit = useCallback(async (data) => {
    try {
      const requestData = {
        id,
        seedId: data.seed,
        blockId: data.block,
        conditionId: data.condition,
        harvest: data.harvest,
        date: data.date,
      };
      await editHarvest(requestData);
      onModalClose();
    } catch (error) {
      if (error instanceof RequestError) {
        toast({
          colorScheme: 'red',
          title: 'Gagal mengubah panen',
          description: error.message,
        });
      }
    }
  }, []);

  const onDelete = useCallback(async () => {
    try {
      await deleteHarvest({ id });
      onAlertClose();
    } catch (error) {
      if (error instanceof RequestError) {
        toast({
          colorScheme: 'red',
          title: 'Gagal menghapus panen',
          description: error.message,
        });
      }
    }
  }, []);

  useEffect(() => {
    methods.reset({
      seed: seed.id,
      block: block.id,
      harvest,
      condition: condition.id,
      date: parsedDate,
    });
  }, [seed, block, condition, harvest, parsedDate]);

  return (
    <Tr>
      <Td>{seed.name}</Td>
      <Td>{block.name}</Td>
      <Td>{condition.name}</Td>
      <Td>{`${harvest} KG`}</Td>
      <Td>{harvestDate}</Td>
      <Td width="0">
        <ButtonGroup>
          <IconButton
            icon={<Icon as={AiOutlineEdit} />}
            colorScheme="yellow"
            onClick={onModalOpen}
            aria-label={`Ubah ${seed.name}`}
          />
          <IconButton
            icon={<Icon as={AiOutlineDelete} />}
            colorScheme="red"
            onClick={onAlertOpen}
            aria-label={`Hapus ${seed.name}`}
          />
        </ButtonGroup>
      </Td>
      <AlertDialog
        isOpen={isAlertOpen}
        leastDestructiveRef={alertRef}
        onClose={onAlertClose}
        closeOnOverlayClick={!isDeleteHarvestLoading}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Hapus Setor Panen
            </AlertDialogHeader>
            <AlertDialogBody>
              Setor Panen{' '}
              <Text as="strong">
                {seed.name} di blok {block.name} pada {harvestDate}
              </Text>{' '}
              akan dihapus dan tidak dapat dikembalikan.
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button
                ref={alertRef}
                onClick={onAlertClose}
                isLoading={isDeleteHarvestLoading}
              >
                Batal
              </Button>
              <Button
                colorScheme="red"
                onClick={onDelete}
                ml={3}
                isLoading={isDeleteHarvestLoading}
              >
                Hapus
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
      <Modal
        size="2xl"
        blockScrollOnMount={false}
        isOpen={isModalOpen}
        onClose={onModalClose}
        closeOnOverlayClick={!isEditHarvestLoading}
      >
        <ModalOverlay />
        <ModalContent>
          <Form withForm={methods} onSubmit={onSubmit}>
            {({ formState, register }) => (
              <>
                <ModalHeader>Ubah Setor Panen</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <VStack alignItems="stretch">
                    <FormControl isInvalid={!!formState.errors.seed}>
                      <FormLabel>Bibit</FormLabel>
                      <Select {...register('seed')}>
                        {seedsData?.data.seeds.map((s) => (
                          <option key={s.id} value={s.id}>
                            {s.name}
                          </option>
                        ))}
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.seed?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.block}>
                      <FormLabel>Blok</FormLabel>
                      <Select {...register('block')}>
                        {blocksData?.data.blocks.map((b) => (
                          <option key={b.id} value={b.id}>
                            {b.name}
                          </option>
                        ))}
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.block?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.harvest}>
                      <FormLabel>Panen</FormLabel>
                      <Input type="number" {...register('harvest')} />
                      <FormErrorMessage>
                        {formState.errors.harvest?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.condition}>
                      <FormLabel>Kondisi</FormLabel>
                      <Select {...register('condition')}>
                        {conditionsData?.data.conditions.map((c) => (
                          <option key={c.id} value={c.id}>
                            {c.name}
                          </option>
                        ))}
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.condition?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.date}>
                      <FormLabel>Tanggal</FormLabel>
                      <Input type="date" {...register('date')} />
                      <FormErrorMessage>
                        {formState.errors.date?.message}
                      </FormErrorMessage>
                    </FormControl>
                  </VStack>
                </ModalBody>
                <ModalFooter>
                  <ButtonGroup>
                    <Button type="button" onClick={onModalClose}>
                      Batal
                    </Button>
                    <Button type="submit" colorScheme="blue">
                      Ubah
                    </Button>
                  </ButtonGroup>
                </ModalFooter>
              </>
            )}
          </Form>
        </ModalContent>
      </Modal>
    </Tr>
  );
}
