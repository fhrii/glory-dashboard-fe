import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const deleteEmployeePresence = ({ id }) =>
  axios.delete(`/presences/${id}`);

export const useDeleteEmployeePresence = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['presences']);
      queryClient.invalidateQueries(['presenceStatistic']);
    },
    mutationFn: deleteEmployeePresence,
    ...config,
  });
