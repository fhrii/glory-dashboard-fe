import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const updateEmployeePresence = ({ id, ...data }) =>
  axios.put(`/presences/${id}`, data);

export const useUpdateEmployeePresence = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['presences']);
      queryClient.invalidateQueries(['presenceStatistic']);
    },
    mutationFn: updateEmployeePresence,
    ...config,
  });
