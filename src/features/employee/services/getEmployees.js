import { useQuery } from 'react-query';

import { useCreatePaginate } from '@/hooks';
import { axios } from '@/libs/axios';
import { emptyStringThenNull } from '@/utils/emptyStringThenNull';

export const getEmployees = ({ status, name, gender, skip, take }) =>
  axios.get('/employees', { params: { status, name, gender, skip, take } });

export const useGetEmployees = ({
  status: oldStatus,
  name: oldName,
  gender: oldGender,
  config,
} = {}) => {
  const { skip, take, setPage } = useCreatePaginate();
  const status = emptyStringThenNull(oldStatus);
  const name = emptyStringThenNull(oldName);
  const gender = emptyStringThenNull(oldGender);

  return {
    setPage,
    ...useQuery({
      queryKey: ['employees', { status, name, gender, skip, take }],
      queryFn: () => getEmployees({ status, name, gender, skip, take }),
      ...config,
    }),
  };
};
