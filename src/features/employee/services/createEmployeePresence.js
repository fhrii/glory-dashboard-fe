import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const createEmployeePresence = (data) => axios.post('/presences', data);

export const useCreateEmployeePresence = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['presences']);
      queryClient.invalidateQueries(['presenceStatistic']);
    },
    mutationFn: createEmployeePresence,
    ...config,
  });
