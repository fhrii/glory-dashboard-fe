import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const updateEmployee = ({ id, ...data }) =>
  axios.put(`/employees/${id}`, data);

export const useUpdateEmployee = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['employees']);
    },
    mutationFn: updateEmployee,
    ...config,
  });
