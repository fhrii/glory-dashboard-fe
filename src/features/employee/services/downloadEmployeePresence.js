import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';

export const downloadEmployeePresence = () =>
  axios.get('/presences/statisticExcel', { responseType: 'blob' });

export const useDownloadEmployeePresence = ({ config } = {}) =>
  useMutation({
    mutationFn: downloadEmployeePresence,
    ...config,
  });
