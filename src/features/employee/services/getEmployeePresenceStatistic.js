import { useQuery } from 'react-query';

import { axios } from '@/libs/axios';

export const getEmployeePresenceStatistic = () =>
  axios.get('/presences/statistic');

export const useGetEmployeePresenceStatistic = ({ config } = {}) =>
  useQuery({
    queryKey: ['presenceStatistic'],
    queryFn: () => getEmployeePresenceStatistic(),
    ...config,
  });
