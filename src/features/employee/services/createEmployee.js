import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const createEmployee = (data) => axios.post('/employees', data);

export const useCreateEmployee = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['employees']);
    },
    mutationFn: createEmployee,
    ...config,
  });
