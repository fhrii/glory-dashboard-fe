import { useQuery } from 'react-query';

import { useCreatePaginate } from '@/hooks';
import { axios } from '@/libs/axios';

export const getEmployeePresences = ({ skip, take }) =>
  axios.get('/presences', { params: { skip, take } });

export const useGetEmployeePresences = ({ config } = {}) => {
  const { skip, take, setPage } = useCreatePaginate();

  return {
    setPage,
    ...useQuery({
      queryKey: ['presences', { skip, take }],
      queryFn: () => getEmployeePresences({ skip, take }),
      ...config,
    }),
  };
};
