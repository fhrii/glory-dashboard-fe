import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const deleteEmployee = ({ id }) => axios.delete(`/employees/${id}`);

export const useDeleteEmployee = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['employees']);
    },
    mutationFn: deleteEmployee,
    ...config,
  });
