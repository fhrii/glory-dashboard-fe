import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Box,
  Button,
  ButtonGroup,
  FormControl,
  FormErrorMessage,
  FormHelperText,
  FormLabel,
  Heading,
  HStack,
  Icon,
  IconButton,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Radio,
  RadioGroup,
  Select,
  Spacer,
  Spinner,
  Table,
  TableCaption,
  TableContainer,
  Tag,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import { useCallback, useEffect, useMemo, useRef } from 'react';
import { Controller } from 'react-hook-form';
import { AiOutlineDelete, AiOutlineEdit, AiOutlineEye } from 'react-icons/ai';
import { Else, If, Then } from 'react-if';
import zod from 'zod';

import { Form, useForm } from '@/components/Form';
import { Pagination } from '@/components/UI';
import { RequestError } from '@/exceptions';
import { dayjs } from '@/libs/dayjs';
import { toast } from '@/utils/toast';

import {
  useCreateEmployeePresence,
  useDeleteEmployeePresence,
  useGetEmployeePresences,
  useGetEmployees,
  useUpdateEmployeePresence,
} from '../services';

const EmployeePresenceSchemaErrorConst = {
  DATE_REQUIRED: 'Tanggal harus diisi',
  PRESENCE_TYPE_ENUM: 'Tipe presensi harus diantara Pagi atau Siang',
};

const AddEmployeePresenceSchema = zod.object({
  date: zod.string().min(1, EmployeePresenceSchemaErrorConst.DATE_REQUIRED),
  presenceTime: zod.enum(
    ['MORNING', 'NOUN'],
    EmployeePresenceSchemaErrorConst.PRESENCE_TYPE_ENUM
  ),
  employeesPresence: zod
    .object({
      employeeId: zod.string(),
      presenceType: zod.enum(['ABSENT', 'PERMIT', 'SICK', 'PRESENCE']),
    })
    .array(),
});

const EditEmployeePresenceSchema = zod.object({
  date: zod.string().min(1, EmployeePresenceSchemaErrorConst.DATE_REQUIRED),
  presenceTime: zod.enum(
    ['MORNING', 'NOUN'],
    EmployeePresenceSchemaErrorConst.PRESENCE_TYPE_ENUM
  ),
  employeesPresence: zod
    .object({
      employeeId: zod.string(),
      morningPresence: zod.enum([
        'ABSENT',
        'PERMIT',
        'SICK',
        'PRESENCE',
        'NONE',
      ]),
      nounPresence: zod.enum(['ABSENT', 'PERMIT', 'SICK', 'PRESENCE', 'NONE']),
    })
    .array(),
});

const defaultPresenceDate = new Date().toISOString().split('T')[0];

const defaultValues = {
  date: defaultPresenceDate,
  presenceTime: 'MORNING',
  employeesPresence: [],
};

export function EmployeePresenceList() {
  const methods = useForm(AddEmployeePresenceSchema, { defaultValues });
  const {
    data: employeesPresenceData,
    isLoading: isEmployeesPresenceDataLoading,
    isError: isEmployeesPresenceDataError,
    setPage: setEmployeesPresenceDataPage,
  } = useGetEmployeePresences();
  const { data: employeesData } = useGetEmployees({ status: 1 });
  const { mutateAsync: addPresence, isLoading: isAddPresenceLoading } =
    useCreateEmployeePresence();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const onModalClose = useCallback(() => {
    onClose();
    methods.reset();
  }, []);

  const onSubmit = useCallback(async (data) => {
    try {
      await addPresence(data);
      onModalClose();
    } catch (error) {
      if (error instanceof RequestError) {
        let { message } = error;
        const presenceTime = data.presenceTime === 'MORNING' ? 'Pagi' : 'Siang';

        if (message.includes('already exist'))
          message = `Presensi untuk tanggal ${data.date} dan tipe presensi ${presenceTime} sudah ada`;

        toast({
          colorScheme: 'red',
          title: 'Gagal menambahkan presensi baru',
          description: message,
        });
      }
    }
  }, []);

  const getPresenceRadioValue = useCallback(
    (value, employeeId) =>
      value[
        value.findIndex(
          (presenceData) => presenceData.employeeId === employeeId
        )
      ]?.presenceType,
    []
  );

  const setPresenceRadioValue = useCallback(
    (presenceValue, value, employeeId, onChange) => {
      const newValue = [...value];
      const presenceIndex = newValue.findIndex(
        (presenceData) => presenceData.employeeId === employeeId
      );

      if (presenceIndex === -1) {
        newValue.push({ employeeId, presenceType: presenceValue });
      } else newValue[presenceIndex].presenceType = presenceValue;

      onChange(newValue);
    },
    []
  );

  return (
    <VStack
      alignItems="stretch"
      bgColor="white"
      rounded="lg"
      border="1px"
      borderColor="gray.200"
      overflow="hidden"
    >
      <HStack padding="4" borderBottom="1px" borderBottomColor="gray.200">
        <Heading as="h3" size="md">
          Presensi Karyawan
        </Heading>
        <Spacer />
        <Button colorScheme="blue" onClick={onOpen}>
          Tambah Presensi
        </Button>
      </HStack>
      <VStack alignItems="stretch" padding="4" spacing="6">
        <TableContainer>
          <Table>
            <Thead>
              <Tr>
                <Th>Tanggal</Th>
                <Th>Presensi Pagi</Th>
                <Th>Presensi Siang</Th>
                <Th>Total Karyawan</Th>
                <Th width="0">Aksi</Th>
              </Tr>
            </Thead>
            <Tbody>
              <If condition={() => !isEmployeesPresenceDataLoading}>
                <Then>
                  <If condition={() => !isEmployeesPresenceDataError}>
                    <Then>
                      <If
                        condition={() =>
                          !!employeesPresenceData &&
                          employeesPresenceData.data.presences.length > 0
                        }
                      >
                        <Then>
                          {employeesPresenceData?.data.presences.map(
                            (employeePresence) => (
                              <EmployeePresenceListTableRow
                                key={employeePresence.id}
                                {...employeePresence}
                              />
                            )
                          )}
                        </Then>
                        <Else>
                          <Tr>
                            <Td colSpan="6" textAlign="center">
                              <Box padding="4" textColor="gray.600">
                                Tidak ada presensi
                              </Box>
                            </Td>
                          </Tr>
                        </Else>
                      </If>
                    </Then>
                    <Else>
                      <Tr>
                        <Td colSpan="6" textAlign="center">
                          <Box padding="4">
                            Terjadi kesalahan. Silahkan refresh browser.
                          </Box>
                        </Td>
                      </Tr>
                    </Else>
                  </If>
                </Then>
                <Else>
                  <Tr>
                    <Td colSpan="6">
                      <HStack padding="4">
                        <Spacer />
                        <Spinner />
                        <Spacer />
                      </HStack>
                    </Td>
                  </Tr>
                </Else>
              </If>
            </Tbody>
          </Table>
        </TableContainer>
        <HStack>
          <Spacer />
          <Pagination
            onPageChange={setEmployeesPresenceDataPage}
            totalPages={employeesPresenceData?.data.totalPages}
          />
        </HStack>
      </VStack>
      <Modal
        size="2xl"
        blockScrollOnMount={false}
        isOpen={isOpen}
        onClose={onModalClose}
        closeOnOverlayClick={!isAddPresenceLoading}
      >
        <ModalOverlay />
        <ModalContent>
          <Form withForm={methods} onSubmit={onSubmit}>
            {({ control, formState, register }) => (
              <>
                <ModalHeader>Tambah Presensi</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <VStack alignItems="stretch">
                    <FormControl isInvalid={!!formState.errors.date}>
                      <FormLabel>Tanggal</FormLabel>
                      <Input type="date" {...register('date')} />
                      <FormErrorMessage>
                        {formState.errors.date?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.presenceTime}>
                      <FormLabel>Tipe Presensi</FormLabel>
                      <Select {...register('presenceTime')}>
                        <option value="MORNING">Pagi</option>
                        <option value="NOUN">Siang</option>
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.presenceTime?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl>
                      <FormLabel>Presensi</FormLabel>
                      <TableContainer>
                        <Table variant="simple">
                          <Thead>
                            <Tr>
                              <Th>Nama</Th>
                              <Th width="0">Absen</Th>
                              <Th width="0">Izin</Th>
                              <Th width="0">Sakit</Th>
                              <Th width="0">Hadir</Th>
                            </Tr>
                          </Thead>
                          <Tbody>
                            <Controller
                              name="employeesPresence"
                              control={control}
                              render={({ field: { value, onChange } }) =>
                                employeesData?.data.employees.map(
                                  ({ id: employeeId, name }) => (
                                    <RadioGroup
                                      key={employeeId}
                                      as={Tr}
                                      value={getPresenceRadioValue(
                                        value,
                                        employeeId
                                      )}
                                      onChange={(presenceValue) =>
                                        setPresenceRadioValue(
                                          presenceValue,
                                          value,
                                          employeeId,
                                          onChange
                                        )
                                      }
                                    >
                                      <Td>{name}</Td>
                                      <Td textAlign="center">
                                        <Radio value="ABSENT" />
                                      </Td>
                                      <Td textAlign="center">
                                        <Radio value="PERMIT" />
                                      </Td>
                                      <Td textAlign="center">
                                        <Radio value="SICK" />
                                      </Td>
                                      <Td textAlign="center">
                                        <Radio value="PRESENCE" />
                                      </Td>
                                    </RadioGroup>
                                  )
                                )
                              }
                            />
                          </Tbody>
                        </Table>
                      </TableContainer>
                      <FormHelperText>
                        Apabila opsi presensi tidak dipilih maka akan dianggap{' '}
                        <Text as="strong">Absen</Text>
                      </FormHelperText>
                    </FormControl>
                  </VStack>
                </ModalBody>
                <ModalFooter>
                  <ButtonGroup>
                    <Button
                      type="button"
                      onClick={onClose}
                      isLoading={isAddPresenceLoading}
                    >
                      Batal
                    </Button>
                    <Button
                      type="submit"
                      colorScheme="blue"
                      isLoading={isAddPresenceLoading}
                    >
                      Tambah
                    </Button>
                  </ButtonGroup>
                </ModalFooter>
              </>
            )}
          </Form>
        </ModalContent>
      </Modal>
    </VStack>
  );
}

function EmployeePresenceListTableRow({ id, date, employeesPresence }) {
  const parsedDate = useMemo(() => dayjs(date).formatYearMonthDate(), [date]);
  const presenceDate = useMemo(
    () => dayjs(date).formatYearMonthNameDate(),
    [date]
  );
  const morningPresenceCount = useMemo(
    () =>
      employeesPresence.filter(
        (employeePresence) => employeePresence.presenceTime === 'MORNING'
      ).length,
    [employeesPresence]
  );
  const nounPresenceCount = useMemo(
    () =>
      employeesPresence.filter(
        (employeePresence) => employeePresence.presenceTime === 'NOUN'
      ).length,
    [employeesPresence]
  );
  const totalEmployee = useMemo(
    () =>
      morningPresenceCount > nounPresenceCount
        ? morningPresenceCount
        : nounPresenceCount,
    [morningPresenceCount, nounPresenceCount]
  );
  const employeesName = useMemo(
    () =>
      employeesPresence
        .filter(
          (employeePresence, index) =>
            employeesPresence.findIndex(
              (aEmployeePresence) =>
                aEmployeePresence.employee.name ===
                employeePresence.employee.name
            ) === index
        )
        .map((employeePresence) => employeePresence.employee.name),
    [employeesPresence]
  );
  const employeesPresenceDetail = useMemo(
    () =>
      employeesName.map((name) => {
        const morningPresenceIndex = employeesPresence.findIndex(
          (employeePresence) =>
            employeePresence.employee.name === name &&
            employeePresence.presenceTime === 'MORNING'
        );
        const nounPresenceIndex = employeesPresence.findIndex(
          (employeePresence) =>
            employeePresence.employee.name === name &&
            employeePresence.presenceTime === 'NOUN'
        );
        const mPresence =
          morningPresenceIndex === -1
            ? 'NONE'
            : employeesPresence[morningPresenceIndex].presenceType;
        const nPresence =
          nounPresenceIndex === -1
            ? 'NONE'
            : employeesPresence[nounPresenceIndex].presenceType;
        return {
          employeeId:
            employeesPresence[
              morningPresenceIndex === -1
                ? nounPresenceIndex
                : morningPresenceIndex
            ].employee.id,
          name,
          morningPresence: mPresence,
          nounPresence: nPresence,
        };
      }),
    [employeesPresence]
  );
  const methods = useForm(EditEmployeePresenceSchema, {
    defaultValues: {
      presenceTime: 'MORNING',
      employeesPresence: employeesPresenceDetail,
      date: parsedDate,
    },
  });
  const { mutateAsync: editPresence, isLoading: isEditPresenceLoading } =
    useUpdateEmployeePresence();
  const { mutateAsync: deletePresence, isLoading: isDeletePresenceLoading } =
    useDeleteEmployeePresence();
  const presenceTimeFormValue = methods.watch('presenceTime');
  const {
    isOpen: isAlertOpen,
    onOpen: onAlertOpen,
    onClose: onAlertClose,
  } = useDisclosure();
  const {
    isOpen: isModalEditOpen,
    onOpen: onModalEditOpen,
    onClose: onModalEditClose,
  } = useDisclosure();
  const {
    isOpen: isModalDetailOpen,
    onOpen: onModalDetailOpen,
    onClose: onModalDetailClose,
  } = useDisclosure();
  const alertRef = useRef();

  const onSubmit = useCallback(async (data) => {
    try {
      const requestData = {
        id,
        presenceTime: data.presenceTime,
        employeesPresence: data.employeesPresence.map(
          ({ employeeId, morningPresence, nounPresence }) => {
            let presenceType =
              data.presenceTime === 'MORNING' ? morningPresence : nounPresence;
            presenceType = presenceType === 'NONE' ? 'ABSENT' : presenceType;
            return {
              employeeId,
              presenceType,
            };
          }
        ),
      };
      await editPresence(requestData);
      onModalEditClose();
    } catch (error) {
      if (error instanceof RequestError) {
        toast({
          colorScheme: 'red',
          title: 'Gagal mengubah presensi',
          description: error.message,
        });
      }
    }
  }, []);

  const onDelete = useCallback(async () => {
    try {
      await deletePresence({ id });
      onAlertClose();
    } catch (error) {
      if (error instanceof RequestError) {
        toast({
          colorScheme: 'red',
          title: 'Gagal menghapus presensi',
          description: error.message,
        });
      }
    }
  }, []);

  const getPresenceRadioValue = useCallback(
    (value, employeeId, presenceTime) => {
      const { morningPresence, nounPresence } =
        value[
          value.findIndex(
            (presenceData) => presenceData.employeeId === employeeId
          )
        ];

      return presenceTime === 'MORNING' ? morningPresence : nounPresence;
    },
    []
  );

  const setPresenceRadioValue = useCallback(
    (presenceValue, value, employeeId, presenceTime, onChange) => {
      const newValue = [...value];
      const presenceIndex = newValue.findIndex(
        (presenceData) => presenceData.employeeId === employeeId
      );

      if (presenceTime === 'MORNING')
        newValue[presenceIndex].morningPresence = presenceValue;
      else if (presenceTime === 'NOUN')
        newValue[presenceIndex].nounPresence = presenceValue;

      onChange(newValue);
    },
    []
  );

  useEffect(() => {
    methods.reset({
      presenceTime: 'MORNING',
      employeesPresence: employeesPresenceDetail,
      date: parsedDate,
    });
  }, [employeesPresenceDetail, parsedDate]);

  return (
    <Tr>
      <Td>{presenceDate}</Td>
      <Td>{morningPresenceCount}</Td>
      <Td>{nounPresenceCount}</Td>
      <Td>{totalEmployee}</Td>
      <Td width="0">
        <ButtonGroup>
          <IconButton
            icon={<Icon as={AiOutlineEye} />}
            colorScheme="teal"
            onClick={onModalDetailOpen}
            aria-label={`Lihat presensi untuk tanggal ${date}`}
          />
          <IconButton
            icon={<Icon as={AiOutlineEdit} />}
            colorScheme="yellow"
            onClick={onModalEditOpen}
            aria-label={`Ubah presensi untuk tanggal ${date}`}
          />
          <IconButton
            icon={<Icon as={AiOutlineDelete} />}
            colorScheme="red"
            onClick={onAlertOpen}
            aria-label={`Hapus presensi untuk tanggal ${date}`}
          />
        </ButtonGroup>
      </Td>
      <AlertDialog
        isOpen={isAlertOpen}
        leastDestructiveRef={alertRef}
        onClose={onAlertClose}
        closeOnOverlayClick={!isDeletePresenceLoading}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Hapus Presensi
            </AlertDialogHeader>
            <AlertDialogBody>
              Presensi untuk tanggal <Text as="strong">{presenceDate}</Text>{' '}
              akan dihapus dan tidak dapat dikembalikan.
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button
                ref={alertRef}
                onClick={onAlertClose}
                isLoading={isDeletePresenceLoading}
              >
                Batal
              </Button>
              <Button
                colorScheme="red"
                onClick={onDelete}
                ml={3}
                isLoading={isDeletePresenceLoading}
              >
                Hapus
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
      <Modal
        size="2xl"
        blockScrollOnMount={false}
        isOpen={isModalEditOpen}
        onClose={onModalEditClose}
        closeOnOverlayClick={!isEditPresenceLoading}
      >
        <ModalOverlay />
        <ModalContent>
          <Form withForm={methods} onSubmit={onSubmit}>
            {({ control, formState, register }) => (
              <>
                <ModalHeader>Ubah Presensi</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <VStack alignItems="stretch">
                    <FormControl isInvalid={!!formState.errors.date} isReadOnly>
                      <FormLabel>Tanggal</FormLabel>
                      <Input type="date" {...register('date')} />
                      <FormErrorMessage>
                        {formState.errors.date?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.presenceTime}>
                      <FormLabel>Tipe Presensi</FormLabel>
                      <Select {...register('presenceTime')}>
                        <option value="MORNING">Pagi</option>
                        <option value="NOUN">Siang</option>
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.presenceTime?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl>
                      <FormLabel>Presensi</FormLabel>
                      <TableContainer>
                        <Table variant="simple">
                          <Thead>
                            <Tr>
                              <Th>Nama</Th>
                              <Th width="0">Absen</Th>
                              <Th width="0">Izin</Th>
                              <Th width="0">Sakit</Th>
                              <Th width="0">Hadir</Th>
                            </Tr>
                          </Thead>
                          <Tbody>
                            <Controller
                              name="employeesPresence"
                              control={control}
                              render={({ field: { value, onChange } }) =>
                                employeesPresenceDetail.map(
                                  ({ employeeId, name }) => (
                                    <RadioGroup
                                      key={employeeId}
                                      as={Tr}
                                      value={getPresenceRadioValue(
                                        value,
                                        employeeId,
                                        presenceTimeFormValue
                                      )}
                                      onChange={(presenceValue) =>
                                        setPresenceRadioValue(
                                          presenceValue,
                                          value,
                                          employeeId,
                                          presenceTimeFormValue,
                                          onChange
                                        )
                                      }
                                    >
                                      <Td>{name}</Td>
                                      <Td textAlign="center">
                                        <Radio value="ABSENT" />
                                      </Td>
                                      <Td textAlign="center">
                                        <Radio value="PERMIT" />
                                      </Td>
                                      <Td textAlign="center">
                                        <Radio value="SICK" />
                                      </Td>
                                      <Td textAlign="center">
                                        <Radio value="PRESENCE" />
                                      </Td>
                                    </RadioGroup>
                                  )
                                )
                              }
                            />
                          </Tbody>
                        </Table>
                      </TableContainer>
                      <FormHelperText>
                        Apabila opsi presensi tidak dipilih maka akan dianggap{' '}
                        <Text as="strong">Absen</Text>
                      </FormHelperText>
                    </FormControl>
                  </VStack>
                </ModalBody>
                <ModalFooter>
                  <ButtonGroup>
                    <Button
                      type="button"
                      onClick={onModalEditClose}
                      isLoading={isEditPresenceLoading}
                    >
                      Batal
                    </Button>
                    <Button
                      type="submit"
                      colorScheme="blue"
                      isLoading={isEditPresenceLoading}
                    >
                      Ubah
                    </Button>
                  </ButtonGroup>
                </ModalFooter>
              </>
            )}
          </Form>
        </ModalContent>
      </Modal>
      <Modal
        size="2xl"
        blockScrollOnMount={false}
        isOpen={isModalDetailOpen}
        onClose={onModalDetailClose}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Detail Presensi</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <TableContainer>
              <Table variant="simple">
                <TableCaption>
                  Presensi karyawan tanggal {presenceDate}
                </TableCaption>
                <Thead>
                  <Tr>
                    <Th>Nama</Th>
                    <Th width="0">Presensi Pagi</Th>
                    <Th width="0">Presensi Siang</Th>
                  </Tr>
                </Thead>
                <Tbody>
                  {employeesPresenceDetail.map((employeePresence) => (
                    <EmployeePresenceDetailTableRow
                      key={employeePresence.employeeId}
                      {...employeePresence}
                    />
                  ))}
                </Tbody>
              </Table>
            </TableContainer>
          </ModalBody>
          <ModalFooter>
            <ButtonGroup>
              <Button onClick={onModalDetailClose}>Tutup</Button>
            </ButtonGroup>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Tr>
  );
}

function EmployeePresenceDetailTableRow({
  name,
  morningPresence,
  nounPresence,
}) {
  const matchPresenceTag = useCallback((presence) => {
    switch (presence) {
      case 'ABSENT':
        return { name: 'Absen', color: 'red' };

      case 'PERMIT':
        return { name: 'Izin', color: 'blue' };

      case 'SICK':
        return { name: 'Sakit', color: 'yellow' };

      case 'PRESENCE':
        return { name: 'Hadir', color: 'green' };

      default:
        return { name: 'Belum Presensi', color: 'gray' };
    }
  }, []);
  const morningPresenceTag = useMemo(
    () => matchPresenceTag(morningPresence),
    [morningPresence]
  );
  const nounPresenceTag = useMemo(
    () => matchPresenceTag(nounPresence),
    [nounPresence]
  );

  return (
    <Tr>
      <Td>{name}</Td>
      <Td>
        <Tag colorScheme={morningPresenceTag.color}>
          {morningPresenceTag.name}
        </Tag>
      </Td>
      <Td>
        <Tag colorScheme={nounPresenceTag.color}>{nounPresenceTag.name}</Tag>
      </Td>
    </Tr>
  );
}
