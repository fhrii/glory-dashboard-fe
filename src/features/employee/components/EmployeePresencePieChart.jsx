import { Heading, VStack } from '@chakra-ui/react';
import dynamic from 'next/dynamic';
import { useMemo } from 'react';

import { dayjs } from '@/libs/dayjs';

import { useGetEmployeePresenceStatistic } from '../services';

const ReactApexChart = dynamic(() => import('react-apexcharts'), {
  ssr: false,
});

export function EmployeePresencePieChart({ label }) {
  const today = useMemo(
    () => dayjs().set('hour', 7).set('minute', 0).set('second', 0),
    []
  );
  const todayDateString = useMemo(() => today.formatYearMonthNameDate(), []);
  const { data: presenceStatisticData } = useGetEmployeePresenceStatistic();
  const presenceSeries = useMemo(() => {
    const noStatistic = [0, 0, 0, 0, 0, 0, 0, 0];

    if (!presenceStatisticData) return noStatistic;

    const todayPresenceStatistic = presenceStatisticData.data.statistic.find(
      (presenceStat) => dayjs(presenceStat.date).unix() === today.unix()
    );

    if (!todayPresenceStatistic) return noStatistic;
    const { presences } = todayPresenceStatistic;

    return [
      presences.morningPresence,
      presences.nounPresence,
      presences.morningPermit,
      presences.nounPermit,
      presences.morningSick,
      presences.nounSick,
      presences.morningAbsent,
      presences.nounAbsent,
    ];
  }, [presenceStatisticData]);
  const options = useMemo(
    () => ({
      chart: {
        type: 'donut',
      },
      labels: [
        'Hadir Pagi',
        'Hadir Siang',
        'Izin Pagi',
        'Izin Siang',
        'Sakit Pagi',
        'Sakit Siang',
        'Tidak Hadir Pagi',
        'Tidak Hadir Siang',
      ],
      dataLabels: {
        enabled: false,
      },
    }),
    []
  );

  return (
    <VStack
      alignItems="stretch"
      padding="4"
      bgColor="white"
      rounded="lg"
      border="1px"
      borderColor="gray.200"
      overflow="hidden"
      spacing="6"
    >
      <Heading as="div" size="md">
        {label || todayDateString}
      </Heading>
      <ReactApexChart
        options={options}
        series={presenceSeries}
        type="donut"
        height={270}
      />
    </VStack>
  );
}
