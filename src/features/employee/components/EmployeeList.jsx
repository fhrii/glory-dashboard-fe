import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Box,
  Button,
  ButtonGroup,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  HStack,
  Icon,
  IconButton,
  Input,
  InputGroup,
  InputLeftElement,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Spacer,
  Spinner,
  Table,
  TableContainer,
  Tag,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import {
  AiOutlineDelete,
  AiOutlineEdit,
  AiOutlineSearch,
} from 'react-icons/ai';
import { Else, If, Then } from 'react-if';
import { useDebounce } from 'use-debounce';
import zod from 'zod';

import { Form, useForm } from '@/components/Form';
import { Pagination } from '@/components/UI';
import { RequestError } from '@/exceptions';
import { toast } from '@/utils/toast';

import {
  useCreateEmployee,
  useDeleteEmployee,
  useGetEmployees,
  useUpdateEmployee,
} from '../services';

const EmployeeSchemaErrorConst = {
  NAME_REQUIRED: 'Nama harus diisi',
  STATUS_ENUM: 'Status harus diantara Aktif atau Tidak Aktif',
  GENDER_ENUM: 'Jenis Kelamin harus diantara Laki-Laki atau Perempuan',
  AGE_MINIMUM: 'Umur harus diisi',
};

const EmployeeSchema = zod.object({
  name: zod.string().min(1, EmployeeSchemaErrorConst.NAME_REQUIRED),
  status: zod.enum(
    ['ACTIVE', 'NOT_ACTIVE'],
    EmployeeSchemaErrorConst.STATUS_ENUM
  ),
  gender: zod.enum(['MAN', 'WOMAN'], EmployeeSchemaErrorConst.GENDER_ENUM),
  age: zod.coerce
    .number({ required_error: 'a' })
    .int()
    .gte(5, EmployeeSchemaErrorConst.AGE_MINIMUM),
});

const defaultValues = {
  status: 'ACTIVE',
  gender: 'MAN',
};

export function EmployeeList() {
  const methods = useForm(EmployeeSchema, { defaultValues });
  const [employeeNameFilter, setEmployeeNameFilter] = useState();
  const [employeeStatusFilter, setEmployeeStatusFilter] = useState();
  const [employeeGenderFilter, setEmployeeGenderFilter] = useState();
  const [debouncedEmployeeNameFilter] = useDebounce(employeeNameFilter, 500);
  const {
    data: employeesData,
    isLoading: isEmployeesDataLoading,
    isError: isEmployeesDataError,
    setPage: setEmployeesDataPage,
  } = useGetEmployees({
    name: debouncedEmployeeNameFilter,
    status: employeeStatusFilter,
    gender: employeeGenderFilter,
  });
  const { mutateAsync: addEmployee, isLoading: isAddEmployeeLoading } =
    useCreateEmployee();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const onEmployeeNameFilterChange = useCallback(
    (event) => setEmployeeNameFilter(event.target.value),
    []
  );

  const onEmployeeStatusFilterChange = useCallback(
    (event) => setEmployeeStatusFilter(event.target.value),
    []
  );

  const onEmployeeGenderFilterChange = useCallback(
    (event) => setEmployeeGenderFilter(event.target.value),
    []
  );

  const onModalClose = useCallback(() => {
    onClose();
    methods.reset();
  }, []);

  const onSubmit = useCallback(async (data) => {
    try {
      await addEmployee(data);
      onModalClose();
    } catch (error) {
      if (error instanceof RequestError) {
        toast({
          colorScheme: 'red',
          title: 'Gagal menambahkan karyawan baru',
          description: error.message,
        });
      }
    }
  }, []);

  return (
    <VStack
      alignItems="stretch"
      bgColor="white"
      rounded="lg"
      border="1px"
      borderColor="gray.200"
      overflow="hidden"
    >
      <HStack padding="4" borderBottom="1px" borderBottomColor="gray.200">
        <Heading as="h3" size="md">
          Karyawan
        </Heading>
        <Spacer />
        <Button colorScheme="blue" onClick={onOpen}>
          Tambah Karyawan
        </Button>
      </HStack>
      <VStack alignItems="stretch" padding="4" spacing="6">
        <HStack>
          <Box>
            <InputGroup size="sm">
              <InputLeftElement pointerEvents="none">
                <Icon as={AiOutlineSearch} />
              </InputLeftElement>
              <Input
                variant="filled"
                placeholder="Cari karyawan"
                rounded="lg"
                value={employeeNameFilter}
                onChange={onEmployeeNameFilterChange}
              />
            </InputGroup>
          </Box>
          <Box>
            <Select
              variant="filled"
              size="sm"
              rounded="lg"
              placeholder="Status"
              value={employeeStatusFilter}
              onChange={onEmployeeStatusFilterChange}
            >
              <option value="0">Tidak Aktif</option>
              <option value="1">Aktif</option>
            </Select>
          </Box>
          <Box>
            <Select
              variant="filled"
              size="sm"
              rounded="lg"
              placeholder="Jenis Kelamin"
              value={employeeGenderFilter}
              onChange={onEmployeeGenderFilterChange}
            >
              <option value="MAN">Laki-laki</option>
              <option value="WOMAN">Perempuan</option>
            </Select>
          </Box>
        </HStack>
        <TableContainer>
          <Table>
            <Thead>
              <Tr>
                <Th>Nama</Th>
                <Th>Status</Th>
                <Th>Jenis Kelamin</Th>
                <Th>Umur</Th>
                <Th width="0">Aksi</Th>
              </Tr>
            </Thead>
            <Tbody>
              <If condition={!isEmployeesDataLoading}>
                <Then>
                  <If condition={!isEmployeesDataError}>
                    <Then>
                      <If
                        condition={
                          !!employeesData &&
                          employeesData.data.employees.length > 0
                        }
                      >
                        <Then>
                          {employeesData?.data.employees.map((employee) => (
                            <EmployeeListTableRow
                              key={employee.id}
                              {...employee}
                            />
                          ))}
                        </Then>
                        <Else>
                          <Tr>
                            <Td colSpan="5" textAlign="center">
                              <Box padding="4" textColor="gray.600">
                                Tidak ada karyawan
                              </Box>
                            </Td>
                          </Tr>
                        </Else>
                      </If>
                    </Then>
                    <Else>
                      <Tr>
                        <Td colSpan="5" textAlign="center">
                          <Box padding="4">
                            Terjadi kesalahan. Silahkan refresh browser.
                          </Box>
                        </Td>
                      </Tr>
                    </Else>
                  </If>
                </Then>
                <Else>
                  <Tr>
                    <Td colSpan="5">
                      <HStack padding="4">
                        <Spacer />
                        <Spinner />
                        <Spacer />
                      </HStack>
                    </Td>
                  </Tr>
                </Else>
              </If>
            </Tbody>
          </Table>
        </TableContainer>
        <HStack>
          <Spacer />
          <Pagination
            onPageChange={setEmployeesDataPage}
            totalPages={employeesData?.data.totalPages}
          />
        </HStack>
      </VStack>
      <Modal
        size="2xl"
        blockScrollOnMount={false}
        isOpen={isOpen}
        onClose={onModalClose}
        onOverlayClick={!isAddEmployeeLoading}
      >
        <ModalOverlay />
        <ModalContent>
          <Form withForm={methods} onSubmit={onSubmit}>
            {({ formState, register }) => (
              <>
                <ModalHeader>Tambah Karyawan</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <VStack alignItems="stretch">
                    <FormControl isInvalid={!!formState.errors.name}>
                      <FormLabel>Nama</FormLabel>
                      <Input {...register('name')} />
                      <FormErrorMessage>
                        {formState.errors.name?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.status}>
                      <FormLabel>Status</FormLabel>
                      <Select {...register('status')}>
                        <option value="ACTIVE">Aktif</option>
                        <option value="NOT_ACTIVE">Tidak Aktif</option>
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.status?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.gender}>
                      <FormLabel>Jenis Kelamin</FormLabel>
                      <Select {...register('gender')}>
                        <option value="MAN">Laki-laki</option>
                        <option value="WOMAN">Perempuan</option>
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.gender?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.age}>
                      <FormLabel>Umur</FormLabel>
                      <Input type="number" {...register('age')} />
                      <FormErrorMessage>
                        {formState.errors.age?.message}
                      </FormErrorMessage>
                    </FormControl>
                  </VStack>
                </ModalBody>
                <ModalFooter>
                  <ButtonGroup>
                    <Button
                      type="button"
                      onClick={onModalClose}
                      isLoading={isAddEmployeeLoading}
                    >
                      Batal
                    </Button>
                    <Button
                      type="submit"
                      colorScheme="blue"
                      isLoading={isAddEmployeeLoading}
                    >
                      Tambah
                    </Button>
                  </ButtonGroup>
                </ModalFooter>
              </>
            )}
          </Form>
        </ModalContent>
      </Modal>
    </VStack>
  );
}

function EmployeeListTableRow({ id, name, status, gender, age }) {
  const methods = useForm(EmployeeSchema, {
    defaultValues: { name, status, gender, age },
  });
  const { mutateAsync: editEmployee, isLoading: isEditEmployeeLoading } =
    useUpdateEmployee();
  const { mutateAsync: deleteEmployee, isLoading: isDeleteEmployeeLoading } =
    useDeleteEmployee();
  const {
    isOpen: isAlertOpen,
    onOpen: onAlertOpen,
    onClose: onAlertClose,
  } = useDisclosure();
  const {
    isOpen: isModalOpen,
    onOpen: onModalOpen,
    onClose: onModalClose,
  } = useDisclosure();
  const alertRef = useRef();
  const statusColor = useMemo(
    () => (status === 'ACTIVE' ? 'green' : 'red'),
    [status]
  );
  const statusName = useMemo(
    () => (status === 'ACTIVE' ? 'Aktif' : 'Tidak Aktif'),
    [status]
  );
  const genderColor = useMemo(() => (gender === 'MAN' ? 'blue' : 'pink'));
  const genderName = useMemo(() =>
    gender === 'MAN' ? 'Laki-laki' : 'Perempuan'
  );

  const onSubmit = useCallback(async (data) => {
    try {
      await editEmployee({ id, ...data });
      onModalClose();
    } catch (error) {
      if (error instanceof RequestError) {
        toast({
          colorScheme: 'red',
          title: 'Gagal mengubah karyawan',
          description: error.message,
        });
      }
    }
  });

  const onDelete = useCallback(async () => {
    try {
      await deleteEmployee({ id });
      onAlertClose();
    } catch (error) {
      if (error instanceof RequestError) {
        toast({
          colorScheme: 'red',
          title: 'Gagal menghapus karyawan',
          description: error.message,
        });
      }
    }
  }, []);

  useEffect(() => {
    methods.reset({ name, status, gender, age });
  }, [name, status, gender, age]);

  return (
    <Tr>
      <Td>{name}</Td>
      <Td>
        <Tag colorScheme={statusColor}>{statusName}</Tag>
      </Td>
      <Td>
        <Tag colorScheme={genderColor}>{genderName}</Tag>
      </Td>
      <Td>{age}</Td>
      <Td width="0">
        <ButtonGroup>
          <IconButton
            icon={<Icon as={AiOutlineEdit} />}
            colorScheme="yellow"
            onClick={onModalOpen}
            aria-label={`Ubah ${name}`}
          />
          <IconButton
            icon={<Icon as={AiOutlineDelete} />}
            colorScheme="red"
            onClick={onAlertOpen}
            aria-label={`Hapus ${name}`}
          />
        </ButtonGroup>
      </Td>
      <AlertDialog
        isOpen={isAlertOpen}
        leastDestructiveRef={alertRef}
        onClose={onAlertClose}
        closeOnOverlayClick={!isDeleteEmployeeLoading}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Hapus Karyawan
            </AlertDialogHeader>
            <AlertDialogBody>
              Karyawan <Text as="strong">{name}</Text> akan dihapus dan tidak
              dapat dikembalikan.
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button
                ref={alertRef}
                onClick={onAlertClose}
                isLoading={isDeleteEmployeeLoading}
              >
                Batal
              </Button>
              <Button
                colorScheme="red"
                onClick={onDelete}
                ml={3}
                isLoading={isDeleteEmployeeLoading}
              >
                Hapus
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
      <Modal
        size="2xl"
        blockScrollOnMount={false}
        isOpen={isModalOpen}
        onClose={onModalClose}
        closeOnOverlayClick={!isDeleteEmployeeLoading}
      >
        <ModalOverlay />
        <ModalContent>
          <Form withForm={methods} onSubmit={onSubmit}>
            {({ formState, register }) => (
              <>
                <ModalHeader>Ubah Karyawan</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <VStack alignItems="stretch">
                    <FormControl isInvalid={!!formState.errors.name}>
                      <FormLabel>Nama</FormLabel>
                      <Input {...register('name')} />
                      <FormErrorMessage>
                        {formState.errors.name?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.status}>
                      <FormLabel>Status</FormLabel>
                      <Select {...register('status')}>
                        <option value="ACTIVE">Aktif</option>
                        <option value="NOT_ACTIVE">Tidak Aktif</option>
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.status?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.gender}>
                      <FormLabel>Jenis Kelamin</FormLabel>
                      <Select {...register('gender')}>
                        <option value="MAN">Laki-laki</option>
                        <option value="WOMAN">Perempuan</option>
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.gender?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.age}>
                      <FormLabel>Umur</FormLabel>
                      <Input type="number" {...register('age')} />
                      <FormErrorMessage>
                        {formState.errors.age?.message}
                      </FormErrorMessage>
                    </FormControl>
                  </VStack>
                </ModalBody>
                <ModalFooter>
                  <ButtonGroup>
                    <Button
                      type="button"
                      onClick={onModalClose}
                      isLoading={isEditEmployeeLoading}
                    >
                      Batal
                    </Button>
                    <Button
                      type="submit"
                      colorScheme="blue"
                      isLoading={isEditEmployeeLoading}
                    >
                      Ubah
                    </Button>
                  </ButtonGroup>
                </ModalFooter>
              </>
            )}
          </Form>
        </ModalContent>
      </Modal>
    </Tr>
  );
}
