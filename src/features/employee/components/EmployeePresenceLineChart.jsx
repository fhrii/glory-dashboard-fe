import { Button, Heading, HStack, Spacer, VStack } from '@chakra-ui/react';
import FileSaver from 'file-saver';
import dynamic from 'next/dynamic';
import { useCallback, useMemo } from 'react';

import { RequestError } from '@/exceptions';
import { dayjs } from '@/libs/dayjs';
import { toast } from '@/utils/toast';

import {
  useDownloadEmployeePresence,
  useGetEmployeePresenceStatistic,
} from '../services';

const ReactApexChart = dynamic(() => import('react-apexcharts'), {
  ssr: false,
});

export function EmployeePresenceLineChart({ label }) {
  const today = useMemo(
    () => dayjs().set('hour', 7).set('minute', 0).set('second', 0),
    []
  );
  const categories = useMemo(
    () => [
      ...Array.from({ length: 29 }, (_, index) =>
        today.subtract(30 - (index + 1), 'day')
      ),
      today,
    ],
    []
  );
  const { data: presenceStatisticData } = useGetEmployeePresenceStatistic();
  const {
    mutateAsync: downloadEmployeePresence,
    isLoading: isDownloadEmployeePresenceLoading,
  } = useDownloadEmployeePresence();
  const presenceSeries = useMemo(() => {
    const presencesData = categories.map((category) => {
      const categoryStatistic = presenceStatisticData?.data.statistic.find(
        (presenceStat) => dayjs(presenceStat.date).unix() === category.unix()
      );
      const presenceData = {
        morningAbsent: 0,
        morningPermit: 0,
        morningSick: 0,
        morningPresence: 0,
        nounAbsent: 0,
        nounPermit: 0,
        nounSick: 0,
        nounPresence: 0,
      };

      if (!categoryStatistic) return presenceData;

      return categoryStatistic.presences;
    });

    return [
      {
        name: 'Hadir Pagi',
        data: presencesData.map((presence) => presence.morningPresence),
      },
      {
        name: 'Hadir Siang',
        data: presencesData.map((presence) => presence.nounPresence),
      },
      {
        name: 'Izin Pagi',
        data: presencesData.map((presence) => presence.morningPermit),
      },
      {
        name: 'Izin Siang',
        data: presencesData.map((presence) => presence.nounPermit),
      },
      {
        name: 'Sakit Pagi',
        data: presencesData.map((presence) => presence.morningSick),
      },
      {
        name: 'Sakit Siang',
        data: presencesData.map((presence) => presence.nounSick),
      },
      {
        name: 'Tidak Hadir Pagi',
        data: presencesData.map((presence) => presence.morningAbsent),
      },
      {
        name: 'Tidak Hadir Siang',
        data: presencesData.map((presence) => presence.nounAbsent),
      },
    ];
  }, [presenceStatisticData]);
  const options = useMemo(
    () => ({
      chart: {
        height: 300,
        type: 'line',
        toolbar: {
          show: false,
        },
        zoom: {
          enabled: false,
        },
        animations: {
          enabled: true,
        },
      },
      stroke: {
        width: Array.from({ length: 30 }, () => 5),
        curve: 'smooth',
      },
      yaxis: [
        {
          labels: {
            formatter(val) {
              return val.toFixed(0);
            },
          },
        },
      ],
      xaxis: {
        categories: categories.map((category) => category.formatMonthDate()),
      },
    }),
    []
  );

  const onButtonExcelClick = useCallback(async () => {
    try {
      const response = await downloadEmployeePresence();
      FileSaver.saveAs(response, `${Date.now()}.xlsx`);
    } catch (error) {
      if (error instanceof RequestError) {
        toast({
          colorScheme: 'red',
          title: 'Gagal menambahkan karyawan baru',
          description: error.message,
        });
      }
    }
  }, []);

  return (
    <VStack
      alignItems="stretch"
      padding="4"
      bgColor="white"
      rounded="lg"
      border="1px"
      borderColor="gray.200"
      overflow="hidden"
      spacing="6"
    >
      <HStack>
        <Heading as="div" size="md">
          {label || '30 Hari Terakhir'}
        </Heading>
        <Spacer />
        <Button
          type="button"
          colorScheme="green"
          size="sm"
          onClick={onButtonExcelClick}
          isLoading={isDownloadEmployeePresenceLoading}
        >
          Excel
        </Button>
      </HStack>
      <ReactApexChart
        options={options}
        series={presenceSeries}
        type="line"
        height={300}
      />
    </VStack>
  );
}
