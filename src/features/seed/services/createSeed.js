import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const createSeed = (data) => axios.post('/seeds', data);

export const useCreateSeed = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['seeds']);
      queryClient.invalidateQueries(['plantStatistic']);
      queryClient.invalidateQueries(['harvestStatistic']);
    },
    mutationFn: createSeed,
    ...config,
  });
