import { useQuery } from 'react-query';

import { useCreatePaginate } from '@/hooks';
import { axios } from '@/libs/axios';
import { emptyStringThenNull } from '@/utils/emptyStringThenNull';

export const getSeeds = ({ status, name, skip, take }) =>
  axios.get('/seeds', { params: { status, name, skip, take } });

export const useGetSeeds = ({
  status: oldStatus,
  name: oldName,
  config,
} = {}) => {
  const { skip, take, setPage } = useCreatePaginate();
  const status = emptyStringThenNull(oldStatus);
  const name = emptyStringThenNull(oldName);

  return {
    setPage,
    ...useQuery({
      queryKey: ['seeds', { name, status, skip, take }],
      queryFn: () => getSeeds({ name, status, skip, take }),
      ...config,
    }),
  };
};
