import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const deleteSeed = ({ id }) => axios.delete(`/seeds/${id}`);

export const useDeleteSeed = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['seeds']);
      queryClient.invalidateQueries(['plantStatistic']);
      queryClient.invalidateQueries(['harvestStatistic']);
    },
    mutationFn: deleteSeed,
    ...config,
  });
