import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const updateSeed = ({ id, ...data }) => axios.put(`/seeds/${id}`, data);

export const useUpdateSeed = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['seeds']);
      queryClient.invalidateQueries(['plantStatistic']);
      queryClient.invalidateQueries(['harvestStatistic']);
    },
    mutationFn: updateSeed,
    ...config,
  });
