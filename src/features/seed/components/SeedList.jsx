import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Box,
  Button,
  ButtonGroup,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  HStack,
  Icon,
  IconButton,
  Input,
  InputGroup,
  InputLeftElement,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Select,
  Spacer,
  Spinner,
  Table,
  TableContainer,
  Tag,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import {
  AiOutlineDelete,
  AiOutlineEdit,
  AiOutlineSearch,
} from 'react-icons/ai';
import { Else, If, Then } from 'react-if';
import { useDebounce } from 'use-debounce';
import zod from 'zod';

import { Form, useForm } from '@/components/Form';
import { Pagination } from '@/components/UI';
import { RequestError } from '@/exceptions';
import { toast } from '@/utils/toast';

import {
  useCreateSeed,
  useDeleteSeed,
  useGetSeeds,
  useUpdateSeed,
} from '../services';

const SeedSchemaErrorConst = {
  NAME_REQUIRED: 'Nama harus diisi',
  STATUS_ENUM: 'Status harus diantara Aktif atau Tidak Aktif',
};

const SeedSchema = zod.object({
  name: zod.string().min(1, SeedSchemaErrorConst.NAME_REQUIRED),
  status: zod.enum(['ACTIVE', 'NOT_ACTIVE'], SeedSchemaErrorConst.STATUS_ENUM),
});

const defaultValues = {
  status: 'ACTIVE',
};

export function SeedList() {
  const methods = useForm(SeedSchema, { defaultValues });
  const [seedNameFilter, setSeedNameFilter] = useState();
  const [seedStatusFilter, setSeedStatusFilter] = useState();
  const [debouncedSeedNameFilter] = useDebounce(seedNameFilter, 500);
  const {
    data: seedsData,
    isLoading: isSeedsDataLoading,
    isError: isSeedsDataError,
    setPage: setSeedsDataPage,
  } = useGetSeeds({ name: debouncedSeedNameFilter, status: seedStatusFilter });
  const { mutateAsync: addSeed, isLoading: isAddSeedLoading } = useCreateSeed();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const onSeedNameFilterChange = useCallback(
    (event) => setSeedNameFilter(event.target.value),
    []
  );

  const onSeedStatusFilterChange = useCallback(
    (event) => setSeedStatusFilter(event.target.value),
    []
  );

  const onModalClose = useCallback(() => {
    onClose();
    methods.reset();
  }, []);

  const onSubmit = useCallback(async (data) => {
    try {
      await addSeed(data);
      onModalClose();
    } catch (error) {
      if (error instanceof RequestError) {
        let { message } = error;

        if (message.includes('already exist'))
          message = `Bibit ${data.name} sudah ada`;

        toast({
          colorScheme: 'red',
          title: 'Gagal menambahkan Bibit baru',
          description: message,
        });
      }
    }
  }, []);

  return (
    <VStack
      alignItems="stretch"
      bgColor="white"
      rounded="lg"
      border="1px"
      borderColor="gray.200"
      overflow="hidden"
    >
      <HStack padding="4" borderBottom="1px" borderBottomColor="gray.200">
        <Heading as="h3" size="md">
          Bibit
        </Heading>
        <Spacer />
        <Button colorScheme="blue" onClick={onOpen}>
          Tambah Bibit
        </Button>
      </HStack>
      <VStack alignItems="stretch" padding="4" spacing="6">
        <HStack>
          <Box>
            <InputGroup size="sm">
              <InputLeftElement pointerEvents="none">
                <Icon as={AiOutlineSearch} />
              </InputLeftElement>
              <Input
                variant="filled"
                placeholder="Cari bibit"
                rounded="lg"
                value={seedNameFilter}
                onChange={onSeedNameFilterChange}
              />
            </InputGroup>
          </Box>
          <Box>
            <Select
              variant="filled"
              size="sm"
              rounded="lg"
              placeholder="Status"
              value={seedStatusFilter}
              onChange={onSeedStatusFilterChange}
            >
              <option value="0">Tidak Aktif</option>
              <option value="1">Aktif</option>
            </Select>
          </Box>
        </HStack>
        <TableContainer>
          <Table>
            <Thead>
              <Tr>
                <Th>Nama</Th>
                <Th>Status</Th>
                <Th width="0">Aksi</Th>
              </Tr>
            </Thead>
            <Tbody>
              <If condition={!isSeedsDataLoading}>
                <Then>
                  <If condition={!isSeedsDataError}>
                    <Then>
                      <If
                        condition={
                          !!seedsData && seedsData.data.seeds.length > 0
                        }
                      >
                        <Then>
                          {seedsData?.data.seeds.map((seed) => (
                            <SeedListTableRow key={seed.id} {...seed} />
                          ))}
                        </Then>
                        <Else>
                          <Tr>
                            <Td colSpan="5" textAlign="center">
                              <Box padding="4" textColor="gray.600">
                                Tidak ada bibit
                              </Box>
                            </Td>
                          </Tr>
                        </Else>
                      </If>
                    </Then>
                    <Else>
                      <Tr>
                        <Td colSpan="5" textAlign="center">
                          <Box padding="4">
                            Terjadi kesalahan. Silahkan refresh browser.
                          </Box>
                        </Td>
                      </Tr>
                    </Else>
                  </If>
                </Then>
                <Else>
                  <Tr>
                    <Td colSpan="3">
                      <HStack padding="4">
                        <Spacer />
                        <Spinner />
                        <Spacer />
                      </HStack>
                    </Td>
                  </Tr>
                </Else>
              </If>
            </Tbody>
          </Table>
        </TableContainer>
        <HStack>
          <Spacer />
          <Pagination
            onPageChange={setSeedsDataPage}
            totalPages={seedsData?.data.totalPages}
          />
        </HStack>
      </VStack>
      <Modal
        size="2xl"
        blockScrollOnMount={false}
        isOpen={isOpen}
        onClose={onClose}
        closeOnOverlayClick={!isAddSeedLoading}
      >
        <ModalOverlay />
        <ModalContent>
          <Form withForm={methods} onSubmit={onSubmit}>
            {({ formState, register }) => (
              <>
                <ModalHeader>Tambah Bibit</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <VStack alignItems="stretch">
                    <FormControl isInvalid={!!formState.errors.name}>
                      <FormLabel>Nama</FormLabel>
                      <Input {...register('name')} />
                      <FormErrorMessage>
                        {formState.errors.name?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.status}>
                      <FormLabel>Status</FormLabel>
                      <Select {...register('status')}>
                        <option value="ACTIVE">Aktif</option>
                        <option value="NOT_ACTIVE">Tidak Aktif</option>
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.status?.message}
                      </FormErrorMessage>
                    </FormControl>
                  </VStack>
                </ModalBody>
                <ModalFooter>
                  <ButtonGroup>
                    <Button
                      type="button"
                      onClick={onClose}
                      isLoading={isAddSeedLoading}
                    >
                      Batal
                    </Button>
                    <Button
                      type="submit"
                      colorScheme="blue"
                      isLoading={isAddSeedLoading}
                    >
                      Tambah
                    </Button>
                  </ButtonGroup>
                </ModalFooter>
              </>
            )}
          </Form>
        </ModalContent>
      </Modal>
    </VStack>
  );
}

function SeedListTableRow({ id, name, status }) {
  const methods = useForm(SeedSchema, { defaultValues: { name, status } });
  const { mutateAsync: editSeed, isLoading: isEditSeedLoading } =
    useUpdateSeed();
  const { mutateAsync: deleteSeed, isLoading: isDeleteSeedLoading } =
    useDeleteSeed();
  const {
    isOpen: isAlertOpen,
    onOpen: onAlertOpen,
    onClose: onAlertClose,
  } = useDisclosure();
  const {
    isOpen: isModalOpen,
    onOpen: onModalOpen,
    onClose: onModalClose,
  } = useDisclosure();
  const alertRef = useRef();
  const statusColor = useMemo(
    () => (status === 'ACTIVE' ? 'green' : 'red'),
    [status]
  );
  const statusName = useMemo(
    () => (status === 'ACTIVE' ? 'Aktif' : 'Tidak Aktif'),
    [status]
  );

  const onSubmit = useCallback(async (data) => {
    try {
      await editSeed({ id, ...data });
      onModalClose();
    } catch (error) {
      if (error instanceof RequestError) {
        let { message } = error;

        if (message.includes('already exist'))
          message = `Bibit ${data.name} sudah ada`;

        toast({
          colorScheme: 'red',
          title: 'Gagal mengubah Bibit',
          description: message,
        });
      }
    }
  }, []);

  const onDelete = useCallback(async () => {
    try {
      await deleteSeed({ id });
      onAlertClose();
    } catch (error) {
      if (error instanceof RequestError) {
        toast({
          colorScheme: 'red',
          title: 'Gagal menghapus Bibit',
          description: error.message,
        });
      }
    }
  }, []);

  useEffect(() => {
    methods.reset();
  }, [name, status]);

  return (
    <Tr>
      <Td>{name}</Td>
      <Td>
        <Tag colorScheme={statusColor}>{statusName}</Tag>
      </Td>
      <Td width="0">
        <ButtonGroup>
          <IconButton
            icon={<Icon as={AiOutlineEdit} />}
            colorScheme="yellow"
            onClick={onModalOpen}
            aria-label={`Ubah ${name}`}
          />
          <IconButton
            icon={<Icon as={AiOutlineDelete} />}
            colorScheme="red"
            onClick={onAlertOpen}
            aria-label={`Hapus ${name}`}
          />
        </ButtonGroup>
      </Td>
      <AlertDialog
        isOpen={isAlertOpen}
        leastDestructiveRef={alertRef}
        onClose={onAlertClose}
        closeOnOverlayClick={!isDeleteSeedLoading}
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Hapus Bibit
            </AlertDialogHeader>
            <AlertDialogBody>
              Bibit <Text as="strong">{name}</Text> akan dihapus dan tidak dapat
              dikembalikan.
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button
                ref={alertRef}
                onClick={onAlertClose}
                isloading={isDeleteSeedLoading}
              >
                Batal
              </Button>
              <Button
                colorScheme="red"
                onClick={onDelete}
                ml={3}
                isLoading={isDeleteSeedLoading}
              >
                Hapus
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
      <Modal
        size="2xl"
        blockScrollOnMount={false}
        isOpen={isModalOpen}
        onClose={onModalClose}
        onCloseComplete={isDeleteSeedLoading}
      >
        <ModalOverlay />
        <ModalContent>
          <Form withForm={methods} onSubmit={onSubmit}>
            {({ formState, register }) => (
              <>
                <ModalHeader>Ubah Bibit</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                  <VStack alignItems="stretch">
                    <FormControl isInvalid={!!formState.errors.name}>
                      <FormLabel>Nama</FormLabel>
                      <Input {...register('name')} />
                      <FormErrorMessage>
                        {formState.errors.name?.message}
                      </FormErrorMessage>
                    </FormControl>
                    <FormControl isInvalid={!!formState.errors.status}>
                      <FormLabel>Status</FormLabel>
                      <Select {...register('status')}>
                        <option value="ACTIVE">Aktif</option>
                        <option value="NOT_ACTIVE">Tidak Aktif</option>
                      </Select>
                      <FormErrorMessage>
                        {formState.errors.status?.message}
                      </FormErrorMessage>
                    </FormControl>
                  </VStack>
                </ModalBody>
                <ModalFooter>
                  <ButtonGroup>
                    <Button
                      type="button"
                      onClick={onModalClose}
                      isLoading={isEditSeedLoading}
                    >
                      Batal
                    </Button>
                    <Button
                      type="submit"
                      colorScheme="blue"
                      isLoading={isEditSeedLoading}
                    >
                      Ubah
                    </Button>
                  </ButtonGroup>
                </ModalFooter>
              </>
            )}
          </Form>
        </ModalContent>
      </Modal>
    </Tr>
  );
}
