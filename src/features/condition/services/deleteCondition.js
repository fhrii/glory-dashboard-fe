import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const deleteCondition = ({ id }) => axios.delete(`/conditions/${id}`);

export const useDeleteCondition = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['conditions']);
      queryClient.invalidateQueries(['harvestStatistic']);
    },
    mutationFn: deleteCondition,
    ...config,
  });
