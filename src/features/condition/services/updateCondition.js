import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const updateCondition = ({ id, ...data }) =>
  axios.put(`/conditions/${id}`, data);

export const useUpdateCondition = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['conditions']);
      queryClient.invalidateQueries(['harvestStatistic']);
    },
    mutationFn: updateCondition,
    ...config,
  });
