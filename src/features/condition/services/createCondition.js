import { useMutation } from 'react-query';

import { axios } from '@/libs/axios';
import { queryClient } from '@/libs/react-query';

export const createCondition = (data) => axios.post('/conditions', data);

export const useCreateCondition = ({ config } = {}) =>
  useMutation({
    onSuccess() {
      queryClient.invalidateQueries(['conditions']);
      queryClient.invalidateQueries(['harvestStatistic']);
    },
    mutationFn: createCondition,
    ...config,
  });
