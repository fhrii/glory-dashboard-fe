import { useQuery } from 'react-query';

import { useCreatePaginate } from '@/hooks';
import { axios } from '@/libs/axios';
import { emptyStringThenNull } from '@/utils/emptyStringThenNull';

export const getConditions = ({ status, name, skip, take }) =>
  axios.get('/conditions', { params: { status, name, skip, take } });

export const useGetConditions = ({
  status: oldStatus,
  name: oldName,
  config,
} = {}) => {
  const { skip, take, setPage } = useCreatePaginate();
  const status = emptyStringThenNull(oldStatus);
  const name = emptyStringThenNull(oldName);

  return {
    setPage,
    ...useQuery({
      queryKey: ['conditions', { status, name, skip, take }],
      queryFn: () => getConditions({ status, name, skip, take }),
      ...config,
    }),
  };
};
